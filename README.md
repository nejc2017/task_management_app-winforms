# TODO 3000

A Task management app created as a study project, similar to Trello.com, but created as a desktop C# Winforms app with custom created controls.

Allows creating and modifying:
- users
- boards
- card columns
- cards

Screenshot example:
![TODO 3000 Boards overview](./image-1.png)
![TODO 3000 board example](./image.png)
