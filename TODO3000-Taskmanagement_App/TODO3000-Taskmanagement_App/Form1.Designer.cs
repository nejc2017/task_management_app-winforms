﻿namespace TODO3000_Taskmanagement_App
{
    partial class frm_TodoMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_TodoMain));
            this.tabControl_main = new System.Windows.Forms.TabControl();
            this.tabPage_boards = new System.Windows.Forms.TabPage();
            this.btn_CreateBoard = new System.Windows.Forms.Button();
            this.lbl_BoardOverview = new System.Windows.Forms.Label();
            this.flowLayoutPanel_ProjectTiles = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage_project = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel_CurrentBoardTab = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_CreateNewProject = new System.Windows.Forms.Button();
            this.btn_SelectProject = new System.Windows.Forms.Button();
            this.panel_CurrentTabTitleButtons = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_boardButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btn_searchTab = new System.Windows.Forms.Button();
            this.btn_AddListTab = new System.Windows.Forms.Button();
            this.lbl_CurrentBoardTitle = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton_File = new System.Windows.Forms.ToolStripDropDownButton();
            this.newUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.advancedSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog_Export = new System.Windows.Forms.SaveFileDialog();
            this.tabControl_main.SuspendLayout();
            this.tabPage_boards.SuspendLayout();
            this.tabPage_project.SuspendLayout();
            this.flowLayoutPanel_CurrentBoardTab.SuspendLayout();
            this.panel_CurrentTabTitleButtons.SuspendLayout();
            this.tableLayoutPanel_boardButtons.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl_main
            // 
            this.tabControl_main.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl_main.Controls.Add(this.tabPage_boards);
            this.tabControl_main.Controls.Add(this.tabPage_project);
            this.tabControl_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_main.HotTrack = true;
            this.tabControl_main.Location = new System.Drawing.Point(0, 0);
            this.tabControl_main.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tabControl_main.Multiline = true;
            this.tabControl_main.Name = "tabControl_main";
            this.tabControl_main.Padding = new System.Drawing.Point(10, 5);
            this.tabControl_main.SelectedIndex = 0;
            this.tabControl_main.Size = new System.Drawing.Size(1222, 748);
            this.tabControl_main.TabIndex = 1;
            // 
            // tabPage_boards
            // 
            this.tabPage_boards.AutoScroll = true;
            this.tabPage_boards.Controls.Add(this.btn_CreateBoard);
            this.tabPage_boards.Controls.Add(this.lbl_BoardOverview);
            this.tabPage_boards.Controls.Add(this.flowLayoutPanel_ProjectTiles);
            this.tabPage_boards.Location = new System.Drawing.Point(4, 4);
            this.tabPage_boards.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tabPage_boards.Name = "tabPage_boards";
            this.tabPage_boards.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tabPage_boards.Size = new System.Drawing.Size(1214, 715);
            this.tabPage_boards.TabIndex = 1;
            this.tabPage_boards.Text = "Boards Overview";
            this.tabPage_boards.UseVisualStyleBackColor = true;
            // 
            // btn_CreateBoard
            // 
            this.btn_CreateBoard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CreateBoard.Location = new System.Drawing.Point(863, 50);
            this.btn_CreateBoard.Name = "btn_CreateBoard";
            this.btn_CreateBoard.Size = new System.Drawing.Size(249, 51);
            this.btn_CreateBoard.TabIndex = 2;
            this.btn_CreateBoard.Text = "+ Create new Board";
            this.btn_CreateBoard.UseVisualStyleBackColor = true;
            this.btn_CreateBoard.Click += new System.EventHandler(this.newBoardToolStripMenuItem_Click);
            // 
            // lbl_BoardOverview
            // 
            this.lbl_BoardOverview.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_BoardOverview.Location = new System.Drawing.Point(181, 50);
            this.lbl_BoardOverview.Name = "lbl_BoardOverview";
            this.lbl_BoardOverview.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbl_BoardOverview.Size = new System.Drawing.Size(503, 41);
            this.lbl_BoardOverview.TabIndex = 1;
            this.lbl_BoardOverview.Text = "Boards Overview";
            // 
            // flowLayoutPanel_ProjectTiles
            // 
            this.flowLayoutPanel_ProjectTiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel_ProjectTiles.AutoSize = true;
            this.flowLayoutPanel_ProjectTiles.Location = new System.Drawing.Point(234, 123);
            this.flowLayoutPanel_ProjectTiles.Name = "flowLayoutPanel_ProjectTiles";
            this.flowLayoutPanel_ProjectTiles.Size = new System.Drawing.Size(878, 960);
            this.flowLayoutPanel_ProjectTiles.TabIndex = 0;
            // 
            // tabPage_project
            // 
            this.tabPage_project.AutoScroll = true;
            this.tabPage_project.Controls.Add(this.flowLayoutPanel_CurrentBoardTab);
            this.tabPage_project.Controls.Add(this.panel_CurrentTabTitleButtons);
            this.tabPage_project.Location = new System.Drawing.Point(4, 4);
            this.tabPage_project.Name = "tabPage_project";
            this.tabPage_project.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_project.Size = new System.Drawing.Size(1214, 715);
            this.tabPage_project.TabIndex = 0;
            this.tabPage_project.Text = "Current Board";
            this.tabPage_project.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel_CurrentBoardTab
            // 
            this.flowLayoutPanel_CurrentBoardTab.AutoSize = true;
            this.flowLayoutPanel_CurrentBoardTab.Controls.Add(this.btn_CreateNewProject);
            this.flowLayoutPanel_CurrentBoardTab.Controls.Add(this.btn_SelectProject);
            this.flowLayoutPanel_CurrentBoardTab.Location = new System.Drawing.Point(10, 125);
            this.flowLayoutPanel_CurrentBoardTab.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel_CurrentBoardTab.Name = "flowLayoutPanel_CurrentBoardTab";
            this.flowLayoutPanel_CurrentBoardTab.Size = new System.Drawing.Size(972, 1220);
            this.flowLayoutPanel_CurrentBoardTab.TabIndex = 4;
            this.flowLayoutPanel_CurrentBoardTab.WrapContents = false;
            // 
            // btn_CreateNewProject
            // 
            this.btn_CreateNewProject.AutoSize = true;
            this.btn_CreateNewProject.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_CreateNewProject.Location = new System.Drawing.Point(3, 3);
            this.btn_CreateNewProject.Name = "btn_CreateNewProject";
            this.btn_CreateNewProject.Size = new System.Drawing.Size(179, 36);
            this.btn_CreateNewProject.TabIndex = 0;
            this.btn_CreateNewProject.Text = "+ Create new Board";
            this.btn_CreateNewProject.UseVisualStyleBackColor = true;
            this.btn_CreateNewProject.Click += new System.EventHandler(this.newBoardToolStripMenuItem_Click);
            // 
            // btn_SelectProject
            // 
            this.btn_SelectProject.AutoSize = true;
            this.btn_SelectProject.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_SelectProject.Location = new System.Drawing.Point(188, 3);
            this.btn_SelectProject.Name = "btn_SelectProject";
            this.btn_SelectProject.Size = new System.Drawing.Size(179, 36);
            this.btn_SelectProject.TabIndex = 1;
            this.btn_SelectProject.Text = "Select a Board";
            this.btn_SelectProject.UseVisualStyleBackColor = true;
            this.btn_SelectProject.Click += new System.EventHandler(this.openBoardToolStripMenuItem_Click);
            // 
            // panel_CurrentTabTitleButtons
            // 
            this.panel_CurrentTabTitleButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_CurrentTabTitleButtons.Controls.Add(this.tableLayoutPanel_boardButtons);
            this.panel_CurrentTabTitleButtons.Controls.Add(this.lbl_CurrentBoardTitle);
            this.panel_CurrentTabTitleButtons.Location = new System.Drawing.Point(0, 29);
            this.panel_CurrentTabTitleButtons.Name = "panel_CurrentTabTitleButtons";
            this.panel_CurrentTabTitleButtons.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panel_CurrentTabTitleButtons.Size = new System.Drawing.Size(1166, 60);
            this.panel_CurrentTabTitleButtons.TabIndex = 8;
            // 
            // tableLayoutPanel_boardButtons
            // 
            this.tableLayoutPanel_boardButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel_boardButtons.ColumnCount = 2;
            this.tableLayoutPanel_boardButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_boardButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_boardButtons.Controls.Add(this.btn_searchTab, 1, 0);
            this.tableLayoutPanel_boardButtons.Controls.Add(this.btn_AddListTab, 0, 0);
            this.tableLayoutPanel_boardButtons.Location = new System.Drawing.Point(885, 9);
            this.tableLayoutPanel_boardButtons.Name = "tableLayoutPanel_boardButtons";
            this.tableLayoutPanel_boardButtons.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.tableLayoutPanel_boardButtons.RowCount = 1;
            this.tableLayoutPanel_boardButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_boardButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_boardButtons.Size = new System.Drawing.Size(278, 41);
            this.tableLayoutPanel_boardButtons.TabIndex = 7;
            // 
            // btn_searchTab
            // 
            this.btn_searchTab.Enabled = false;
            this.btn_searchTab.Location = new System.Drawing.Point(142, 6);
            this.btn_searchTab.Name = "btn_searchTab";
            this.btn_searchTab.Size = new System.Drawing.Size(133, 24);
            this.btn_searchTab.TabIndex = 1;
            this.btn_searchTab.Text = "🔍 Open Search";
            this.btn_searchTab.UseVisualStyleBackColor = true;
            this.btn_searchTab.Click += new System.EventHandler(this.advancedSearchToolStripMenuItem_Click);
            // 
            // btn_AddListTab
            // 
            this.btn_AddListTab.Enabled = false;
            this.btn_AddListTab.Location = new System.Drawing.Point(3, 6);
            this.btn_AddListTab.Name = "btn_AddListTab";
            this.btn_AddListTab.Size = new System.Drawing.Size(133, 24);
            this.btn_AddListTab.TabIndex = 0;
            this.btn_AddListTab.Text = "+ Add another list";
            this.btn_AddListTab.UseVisualStyleBackColor = true;
            this.btn_AddListTab.Click += new System.EventHandler(this.btn_AddListTab_Click);
            // 
            // lbl_CurrentBoardTitle
            // 
            this.lbl_CurrentBoardTitle.AutoSize = true;
            this.lbl_CurrentBoardTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CurrentBoardTitle.Location = new System.Drawing.Point(6, 9);
            this.lbl_CurrentBoardTitle.Name = "lbl_CurrentBoardTitle";
            this.lbl_CurrentBoardTitle.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.lbl_CurrentBoardTitle.Size = new System.Drawing.Size(546, 35);
            this.lbl_CurrentBoardTitle.TabIndex = 5;
            this.lbl_CurrentBoardTitle.Text = "Please select a board or create a new one";
            this.lbl_CurrentBoardTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripDropDownButton_File,
            this.toolStripDropDownButton1,
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 3, 1, 0);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(1222, 30);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripDropDownButton_File
            // 
            this.toolStripDropDownButton_File.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserToolStripMenuItem,
            this.switchUserToolStripMenuItem,
            this.newBoardToolStripMenuItem,
            this.openBoardToolStripMenuItem,
            this.exportToolStripMenuItem1,
            this.exitToolStripMenuItem1});
            this.toolStripDropDownButton_File.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_File.Image")));
            this.toolStripDropDownButton_File.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_File.Name = "toolStripDropDownButton_File";
            this.toolStripDropDownButton_File.Size = new System.Drawing.Size(46, 24);
            this.toolStripDropDownButton_File.Text = "File";
            // 
            // newUserToolStripMenuItem
            // 
            this.newUserToolStripMenuItem.Image = global::TODO3000_Taskmanagement_App.Properties.Resources.AddUser_16x;
            this.newUserToolStripMenuItem.Name = "newUserToolStripMenuItem";
            this.newUserToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.newUserToolStripMenuItem.Text = "New User";
            this.newUserToolStripMenuItem.Click += new System.EventHandler(this.newUserToolStripMenuItem_Click);
            // 
            // switchUserToolStripMenuItem
            // 
            this.switchUserToolStripMenuItem.Image = global::TODO3000_Taskmanagement_App.Properties.Resources.UserGroup_16x;
            this.switchUserToolStripMenuItem.Name = "switchUserToolStripMenuItem";
            this.switchUserToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.switchUserToolStripMenuItem.Text = "Switch User";
            this.switchUserToolStripMenuItem.Click += new System.EventHandler(this.switchUserToolStripMenuItem_Click);
            // 
            // newBoardToolStripMenuItem
            // 
            this.newBoardToolStripMenuItem.Image = global::TODO3000_Taskmanagement_App.Properties.Resources.NewFile_16x;
            this.newBoardToolStripMenuItem.Name = "newBoardToolStripMenuItem";
            this.newBoardToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.newBoardToolStripMenuItem.Text = "New Board";
            this.newBoardToolStripMenuItem.Click += new System.EventHandler(this.newBoardToolStripMenuItem_Click);
            // 
            // openBoardToolStripMenuItem
            // 
            this.openBoardToolStripMenuItem.Image = global::TODO3000_Taskmanagement_App.Properties.Resources.ListFolderOpen_16x;
            this.openBoardToolStripMenuItem.Name = "openBoardToolStripMenuItem";
            this.openBoardToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.openBoardToolStripMenuItem.Text = "Open/Manage Board";
            this.openBoardToolStripMenuItem.Click += new System.EventHandler(this.openBoardToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem1
            // 
            this.exportToolStripMenuItem1.Name = "exportToolStripMenuItem1";
            this.exportToolStripMenuItem1.Size = new System.Drawing.Size(249, 26);
            this.exportToolStripMenuItem1.Text = "Export Board to Textfile";
            this.exportToolStripMenuItem1.Click += new System.EventHandler(this.exportToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(249, 26);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.advancedSearchToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(58, 24);
            this.toolStripDropDownButton1.Text = "Tools";
            // 
            // advancedSearchToolStripMenuItem
            // 
            this.advancedSearchToolStripMenuItem.Name = "advancedSearchToolStripMenuItem";
            this.advancedSearchToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.advancedSearchToolStripMenuItem.Text = "Find To Do";
            this.advancedSearchToolStripMenuItem.Click += new System.EventHandler(this.advancedSearchToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(54, 24);
            this.toolStripButton1.Text = "About";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // saveFileDialog_Export
            // 
            this.saveFileDialog_Export.Filter = "\"txt files (*.txt)|*.txt|All files (*.*)|*.*\"";
            this.saveFileDialog_Export.Title = "Export TXT file to";
            // 
            // frm_TodoMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1222, 748);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl_main);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_TodoMain";
            this.Text = "TO DO 3000";
            this.Load += new System.EventHandler(this.frm_TodoMain_Load);
            this.tabControl_main.ResumeLayout(false);
            this.tabPage_boards.ResumeLayout(false);
            this.tabPage_boards.PerformLayout();
            this.tabPage_project.ResumeLayout(false);
            this.tabPage_project.PerformLayout();
            this.flowLayoutPanel_CurrentBoardTab.ResumeLayout(false);
            this.flowLayoutPanel_CurrentBoardTab.PerformLayout();
            this.panel_CurrentTabTitleButtons.ResumeLayout(false);
            this.panel_CurrentTabTitleButtons.PerformLayout();
            this.tableLayoutPanel_boardButtons.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl_main;
        private System.Windows.Forms.TabPage tabPage_boards;
        private System.Windows.Forms.TabPage tabPage_project;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem advancedSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_File;
        private System.Windows.Forms.ToolStripMenuItem switchUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private CustomControl.CustomListControl customListControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_ProjectTiles;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Label lbl_BoardOverview;
        private System.Windows.Forms.Label lbl_CurrentBoardTitle;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_CurrentBoardTab;
        private System.Windows.Forms.ToolStripMenuItem newUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newBoardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openBoardToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_Export;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_boardButtons;
        private System.Windows.Forms.Button btn_searchTab;
        private System.Windows.Forms.Button btn_AddListTab;
        private System.Windows.Forms.Panel panel_CurrentTabTitleButtons;
        private System.Windows.Forms.Button btn_SelectProject;
        private System.Windows.Forms.Button btn_CreateBoard;
        private System.Windows.Forms.Button btn_CreateNewProject;
    }
}

