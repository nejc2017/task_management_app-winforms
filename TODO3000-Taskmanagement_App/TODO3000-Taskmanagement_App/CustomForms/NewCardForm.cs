﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    
    public partial class frm_NewCardForm : Form
    {
        // nov TODO objekt, ki ga bomo poslali s formo

        public ToDo CreatedTodo;
        public CardList CardList;
        public frm_NewCardForm()
        {
            InitializeComponent();
        }

        public frm_NewCardForm(CardList clist)
        {
            InitializeComponent();
            // pošljemo seznam zaradi IDja
            CardList = clist;

        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btn_CardAdd_Click(object sender, EventArgs e)
        {

            // osnovno preverjanje, če todo objekt smemo shraniti
            if (!string.IsNullOrEmpty(tb_CardTitle.Text))
            {
                CreatedTodo = new ToDo();
                CreatedTodo.Title = tb_CardTitle.Text;
                CreatedTodo.Description = tb_Description.Text;

                //ListID
                CreatedTodo.ListID = CardList.ListID;

                int priority;
                switch (comboBox_Priority.Text)
                {
                    case "low": priority = 0;
                    break;

                    case "medium": priority = 1;
                    break;

                    case "high": priority = 2;
                    break;

                    default: priority = 1;
                    break;
                }
                CreatedTodo.Priority = priority;

                CreatedTodo.DateDue = dateTimePicker_DateDue.Value;
                //todo.Position = todoPosition;

                //preveri, če je task končan. Shrani datum, ko je označen kot zaključen
                if (cb_IsFinished.Checked)
                {
                    CreatedTodo.DateFinished = DateTime.Now;
                }

                // SQLite
                //shrani nov objekt v sqlite bazo
                CreatedTodo.Persist();

                // sporočimo "OK", da se forma zapre
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                errorProvider1.SetError(tb_CardTitle, "Please enter a Card Title");
            }

        }

        private void frm_NewCardForm_Load(object sender, EventArgs e)
        {
            comboBox_Priority.SelectedIndex = 1;
            dateTimePicker_DateDue.Checked = false;
        }
    }
}
