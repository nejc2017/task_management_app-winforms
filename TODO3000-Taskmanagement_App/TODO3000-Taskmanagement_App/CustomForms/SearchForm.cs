﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_SearchForm : Form
    {
        Project currentProject;
        public frm_SearchForm()
        {
            InitializeComponent();
        }
        public frm_SearchForm(Project project)
        {
            InitializeComponent();
            currentProject = project;
            // pridobi vse todoje iz trenutnega projekta
            dataGridView_SearchResults.DataSource = ToDo.GetAllTodosByProjectID(currentProject.ProjectID);

            // skrij stolpce v datagridviewu
            dataGridView_SearchResults.Columns["Position"].Visible = false;
            dataGridView_SearchResults.Columns["ToDoID"].Visible = false;
            dataGridView_SearchResults.Columns["Position"].Visible = false;
        }


        //Search as you type: https://www.codeproject.com/Articles/138595/Search-As-You-Type-in-C
        //https://www.aspsnippets.com/Articles/Implement-Search-function-in-Windows-Forms-Application-using-C-and-VBNet.aspx

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            dataGridView_SearchResults.DataSource = ToDo.SearchTodosByProjectID(currentProject.ProjectID, textBox1.Text);

        }

        private void frm_SearchForm_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
            this.StartPosition = FormStartPosition.CenterScreen;


        }


        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            // vsakič ko pritisnemo tipko
            dataGridView_SearchResults.DataSource = ToDo.SearchTodosByProjectID(currentProject.ProjectID, textBox1.Text);

        }
    }
}
