﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OptionsForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_DefaultColor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_CardDescriptionColor = new System.Windows.Forms.Button();
            this.btn_CardTitleColor = new System.Windows.Forms.Button();
            this.lbl_Colors = new System.Windows.Forms.Label();
            this.btn_ListTitleColor = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_DefaultColor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_CardDescriptionColor);
            this.groupBox1.Controls.Add(this.btn_CardTitleColor);
            this.groupBox1.Controls.Add(this.lbl_Colors);
            this.groupBox1.Controls.Add(this.btn_ListTitleColor);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 164);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Color Scheme";
            // 
            // btn_DefaultColor
            // 
            this.btn_DefaultColor.Location = new System.Drawing.Point(42, 135);
            this.btn_DefaultColor.Name = "btn_DefaultColor";
            this.btn_DefaultColor.Size = new System.Drawing.Size(75, 23);
            this.btn_DefaultColor.TabIndex = 6;
            this.btn_DefaultColor.Text = "Default";
            this.btn_DefaultColor.UseVisualStyleBackColor = true;
            this.btn_DefaultColor.Click += new System.EventHandler(this.btn_DefaultColor_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Card Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Card Title";
            // 
            // btn_CardDescriptionColor
            // 
            this.btn_CardDescriptionColor.Location = new System.Drawing.Point(137, 98);
            this.btn_CardDescriptionColor.Name = "btn_CardDescriptionColor";
            this.btn_CardDescriptionColor.Size = new System.Drawing.Size(23, 23);
            this.btn_CardDescriptionColor.TabIndex = 3;
            this.btn_CardDescriptionColor.UseVisualStyleBackColor = true;
            this.btn_CardDescriptionColor.Click += new System.EventHandler(this.btn_CardDescriptionColor_Click);
            // 
            // btn_CardTitleColor
            // 
            this.btn_CardTitleColor.Location = new System.Drawing.Point(137, 69);
            this.btn_CardTitleColor.Name = "btn_CardTitleColor";
            this.btn_CardTitleColor.Size = new System.Drawing.Size(23, 23);
            this.btn_CardTitleColor.TabIndex = 2;
            this.btn_CardTitleColor.UseVisualStyleBackColor = true;
            this.btn_CardTitleColor.Click += new System.EventHandler(this.btn_CardTitleColor_Click);
            // 
            // lbl_Colors
            // 
            this.lbl_Colors.AutoSize = true;
            this.lbl_Colors.Location = new System.Drawing.Point(17, 43);
            this.lbl_Colors.Name = "lbl_Colors";
            this.lbl_Colors.Size = new System.Drawing.Size(61, 17);
            this.lbl_Colors.TabIndex = 1;
            this.lbl_Colors.Text = "List Title";
            // 
            // btn_ListTitleColor
            // 
            this.btn_ListTitleColor.Location = new System.Drawing.Point(137, 43);
            this.btn_ListTitleColor.Name = "btn_ListTitleColor";
            this.btn_ListTitleColor.Size = new System.Drawing.Size(23, 23);
            this.btn_ListTitleColor.TabIndex = 0;
            this.btn_ListTitleColor.UseVisualStyleBackColor = true;
            this.btn_ListTitleColor.Click += new System.EventHandler(this.btn_ListTitleColor_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Location = new System.Drawing.Point(15, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 70);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database ";
            this.groupBox2.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(28, 31);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Delete all data";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // frm_OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 197);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OptionsForm";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.frm_OptionsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_CardDescriptionColor;
        private System.Windows.Forms.Button btn_CardTitleColor;
        private System.Windows.Forms.Label lbl_Colors;
        private System.Windows.Forms.Button btn_ListTitleColor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btn_DefaultColor;
    }
}