﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_AboutForm : Form
    {
        public frm_AboutForm()
        {
            InitializeComponent();
        }

        private void linklbl_Website_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // odpri privzeti brskalnik
            System.Diagnostics.Process.Start("mailto:nejc@lotrič.com");
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
