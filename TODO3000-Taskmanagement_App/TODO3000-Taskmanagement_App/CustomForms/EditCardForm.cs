﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_EditCardForm : Form
    {
        public ToDo todo; // objekt lahko vrnemo nazaj CustomCard kontroli, da prikaže vrednosti
        public frm_EditCardForm()
        {
            InitializeComponent();
        }

        public frm_EditCardForm(ToDo todo)
        {
            InitializeComponent();
            this.todo = todo;
            tb_CardTitle.Text = this.todo.Title;
            tb_Description.Text = this.todo.Description;
            dateTimePicker_DateDue.Value = this.todo.DateDue;
            if (this.todo.DateFinished > DateTime.MinValue)
            {
                cb_IsFinished.Checked = true;
            }
            comboBox_Priority.SelectedIndex = this.todo.Priority;

        }


        private void frm_EditCardForm_Load(object sender, EventArgs e)
        {

        }

        private void btn_cardDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this to do?", "Delete to do",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // zbriši todo
                todo.DeleteTodoandComments();
            }
            this.Close();
            DialogResult = DialogResult.Yes;

        }

        private void btn_CardAdd_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(tb_CardTitle.Text))
            {
                todo.Title = tb_CardTitle.Text;
                todo.Description = tb_Description.Text;

                int priority;

                switch (comboBox_Priority.Text)
                {
                    case "low":
                        priority = 0;
                        break;

                    case "medium":
                        priority = 1;
                        break;

                    case "high":
                        priority = 2;
                        break;

                    default:
                        priority = 1;
                        break;
                }
                todo.Priority = priority;

                todo.DateDue = dateTimePicker_DateDue.Value;
                //_todo.Position = todoPosition;
                //_todo.ListID = ListID;

                //preveri, če je task končan. Shrani datum, ko je označen kot zaključen
                if (cb_IsFinished.Checked)
                {
                    todo.DateFinished = DateTime.Now;
                }

                todo.Persist();
                this.DialogResult = DialogResult.OK;

            }
        }
    }
}
