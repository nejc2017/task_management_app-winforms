﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_EditListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EditListForm));
            this.lblTitle = new System.Windows.Forms.Label();
            this.tb_ListTitle = new System.Windows.Forms.TextBox();
            this.btn_cardDelete = new System.Windows.Forms.Button();
            this.btn_CardAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 17);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Title";
            // 
            // tb_ListTitle
            // 
            this.tb_ListTitle.Location = new System.Drawing.Point(84, 6);
            this.tb_ListTitle.Name = "tb_ListTitle";
            this.tb_ListTitle.Size = new System.Drawing.Size(115, 22);
            this.tb_ListTitle.TabIndex = 13;
            // 
            // btn_cardDelete
            // 
            this.btn_cardDelete.AutoSize = true;
            this.btn_cardDelete.Location = new System.Drawing.Point(84, 34);
            this.btn_cardDelete.Name = "btn_cardDelete";
            this.btn_cardDelete.Size = new System.Drawing.Size(115, 27);
            this.btn_cardDelete.TabIndex = 22;
            this.btn_cardDelete.Text = "❌ Delete List";
            this.btn_cardDelete.UseVisualStyleBackColor = true;
            this.btn_cardDelete.Click += new System.EventHandler(this.btn_cardDelete_Click);
            // 
            // btn_CardAdd
            // 
            this.btn_CardAdd.AutoSize = true;
            this.btn_CardAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_CardAdd.Location = new System.Drawing.Point(12, 34);
            this.btn_CardAdd.Name = "btn_CardAdd";
            this.btn_CardAdd.Size = new System.Drawing.Size(66, 27);
            this.btn_CardAdd.TabIndex = 21;
            this.btn_CardAdd.Text = "Save";
            this.btn_CardAdd.UseVisualStyleBackColor = true;
            this.btn_CardAdd.Click += new System.EventHandler(this.btn_CardAdd_Click);
            // 
            // frm_EditListForm
            // 
            this.AcceptButton = this.btn_CardAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 74);
            this.Controls.Add(this.btn_cardDelete);
            this.Controls.Add(this.btn_CardAdd);
            this.Controls.Add(this.tb_ListTitle);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_EditListForm";
            this.Text = "Edit list";
            this.Load += new System.EventHandler(this.frm_EditListForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox tb_ListTitle;
        private System.Windows.Forms.Button btn_cardDelete;
        private System.Windows.Forms.Button btn_CardAdd;
    }
}