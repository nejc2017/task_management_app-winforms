﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class ProjectSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSelectForm));
            this.listBox_Projects = new System.Windows.Forms.ListBox();
            this.btnSelectProject = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_DeleteProject = new System.Windows.Forms.Button();
            this.btn_Rename = new System.Windows.Forms.Button();
            this.tb_ProjectTitle = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox_Projects
            // 
            this.listBox_Projects.FormattingEnabled = true;
            this.listBox_Projects.ItemHeight = 16;
            this.listBox_Projects.Items.AddRange(new object[] {
            "Weekly TODOs",
            "Shopping List",
            "Birthday Plans",
            "Work Project"});
            this.listBox_Projects.Location = new System.Drawing.Point(12, 33);
            this.listBox_Projects.Name = "listBox_Projects";
            this.listBox_Projects.Size = new System.Drawing.Size(158, 100);
            this.listBox_Projects.TabIndex = 0;
            this.listBox_Projects.SelectedIndexChanged += new System.EventHandler(this.listBox_Projects_SelectedIndexChanged);
            // 
            // btnSelectProject
            // 
            this.btnSelectProject.AutoSize = true;
            this.btnSelectProject.Location = new System.Drawing.Point(12, 139);
            this.btnSelectProject.Name = "btnSelectProject";
            this.btnSelectProject.Size = new System.Drawing.Size(158, 33);
            this.btnSelectProject.TabIndex = 1;
            this.btnSelectProject.Text = "Open Board";
            this.btnSelectProject.UseVisualStyleBackColor = true;
            this.btnSelectProject.Click += new System.EventHandler(this.SelectProject_click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select a Board:";
            // 
            // btn_DeleteProject
            // 
            this.btn_DeleteProject.AutoSize = true;
            this.btn_DeleteProject.Location = new System.Drawing.Point(181, 100);
            this.btn_DeleteProject.Name = "btn_DeleteProject";
            this.btn_DeleteProject.Size = new System.Drawing.Size(157, 32);
            this.btn_DeleteProject.TabIndex = 3;
            this.btn_DeleteProject.Text = "❌ Delete Board";
            this.btn_DeleteProject.UseVisualStyleBackColor = true;
            this.btn_DeleteProject.Click += new System.EventHandler(this.btn_DeleteProject_Click);
            // 
            // btn_Rename
            // 
            this.btn_Rename.AutoSize = true;
            this.btn_Rename.Location = new System.Drawing.Point(181, 61);
            this.btn_Rename.Name = "btn_Rename";
            this.btn_Rename.Size = new System.Drawing.Size(157, 33);
            this.btn_Rename.TabIndex = 4;
            this.btn_Rename.Text = "Rename Board";
            this.btn_Rename.UseVisualStyleBackColor = true;
            this.btn_Rename.Click += new System.EventHandler(this.btn_Rename_Click);
            // 
            // tb_ProjectTitle
            // 
            this.tb_ProjectTitle.Location = new System.Drawing.Point(181, 33);
            this.tb_ProjectTitle.Name = "tb_ProjectTitle";
            this.tb_ProjectTitle.Size = new System.Drawing.Size(157, 22);
            this.tb_ProjectTitle.TabIndex = 5;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ProjectSelectForm
            // 
            this.AcceptButton = this.btnSelectProject;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 182);
            this.Controls.Add(this.tb_ProjectTitle);
            this.Controls.Add(this.btn_Rename);
            this.Controls.Add(this.btn_DeleteProject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSelectProject);
            this.Controls.Add(this.listBox_Projects);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectSelectForm";
            this.Text = "Select a Board to open";
            this.Load += new System.EventHandler(this.ProjectSelectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Projects;
        private System.Windows.Forms.Button btnSelectProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_DeleteProject;
        private System.Windows.Forms.Button btn_Rename;
        private System.Windows.Forms.TextBox tb_ProjectTitle;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}