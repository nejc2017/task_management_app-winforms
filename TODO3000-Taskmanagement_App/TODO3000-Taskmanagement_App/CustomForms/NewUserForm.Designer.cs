﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_NewUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_NewUserForm));
            this.tb_FirstName = new System.Windows.Forms.TextBox();
            this.tb_LastName = new System.Windows.Forms.TextBox();
            this.lbl_Ime = new System.Windows.Forms.Label();
            this.lbl_Priimek = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_CreateUser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_FirstName
            // 
            this.tb_FirstName.Location = new System.Drawing.Point(125, 42);
            this.tb_FirstName.Name = "tb_FirstName";
            this.tb_FirstName.Size = new System.Drawing.Size(99, 22);
            this.tb_FirstName.TabIndex = 11;
            // 
            // tb_LastName
            // 
            this.tb_LastName.Location = new System.Drawing.Point(125, 70);
            this.tb_LastName.Name = "tb_LastName";
            this.tb_LastName.Size = new System.Drawing.Size(99, 22);
            this.tb_LastName.TabIndex = 12;
            // 
            // lbl_Ime
            // 
            this.lbl_Ime.AutoSize = true;
            this.lbl_Ime.Location = new System.Drawing.Point(12, 45);
            this.lbl_Ime.Name = "lbl_Ime";
            this.lbl_Ime.Size = new System.Drawing.Size(76, 17);
            this.lbl_Ime.TabIndex = 13;
            this.lbl_Ime.Text = "First Name";
            // 
            // lbl_Priimek
            // 
            this.lbl_Priimek.AutoSize = true;
            this.lbl_Priimek.Location = new System.Drawing.Point(12, 71);
            this.lbl_Priimek.Name = "lbl_Priimek";
            this.lbl_Priimek.Size = new System.Drawing.Size(76, 17);
            this.lbl_Priimek.TabIndex = 14;
            this.lbl_Priimek.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(122, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "User Profile";
            // 
            // btn_CreateUser
            // 
            this.btn_CreateUser.AutoSize = true;
            this.btn_CreateUser.Location = new System.Drawing.Point(125, 98);
            this.btn_CreateUser.Name = "btn_CreateUser";
            this.btn_CreateUser.Size = new System.Drawing.Size(99, 34);
            this.btn_CreateUser.TabIndex = 16;
            this.btn_CreateUser.Text = "Create User";
            this.btn_CreateUser.UseVisualStyleBackColor = true;
            this.btn_CreateUser.Click += new System.EventHandler(this.btn_CreateUser_Click);
            // 
            // frm_NewUserForm
            // 
            this.AcceptButton = this.btn_CreateUser;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 139);
            this.Controls.Add(this.btn_CreateUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Priimek);
            this.Controls.Add(this.lbl_Ime);
            this.Controls.Add(this.tb_LastName);
            this.Controls.Add(this.tb_FirstName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_NewUserForm";
            this.Text = "New User";
            this.Load += new System.EventHandler(this.frm_NewUserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_FirstName;
        private System.Windows.Forms.TextBox tb_LastName;
        private System.Windows.Forms.Label lbl_Ime;
        private System.Windows.Forms.Label lbl_Priimek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_CreateUser;
    }
}