﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_OptionsForm : Form
    {
        public frm_OptionsForm()
        {
            InitializeComponent();
        }

        private void frm_OptionsForm_Load(object sender, EventArgs e)
        {
            btn_ListTitleColor.BackColor = Properties.Settings.Default.ListTitleColor;
            btn_CardTitleColor.BackColor = Properties.Settings.Default.CardTitleColor;
            btn_CardDescriptionColor.BackColor = Properties.Settings.Default.CardDescriptionColor;
        }

        private void btn_DefaultColor_Click(object sender, EventArgs e)
        {
            // zamenjaj na default barve
            Properties.Settings.Default.ListTitleColor = Globals.ListTitleColor;
            Properties.Settings.Default.CardTitleColor = Globals.CardTitleColor;
            Properties.Settings.Default.CardDescriptionColor = Globals.ListDescriptionColor;


            // prikazi na gumbih
            btn_ListTitleColor.BackColor = Properties.Settings.Default.ListTitleColor;
            btn_CardTitleColor.BackColor = Properties.Settings.Default.CardTitleColor;
            btn_CardDescriptionColor.BackColor = Properties.Settings.Default.CardDescriptionColor;
            Properties.Settings.Default.Save();

            MessageBox.Show("Default colors set. \nPlease restart the application for the changes to take effect.", "Color Scheme", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void btn_ListTitleColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btn_ListTitleColor.BackColor = colorDialog1.Color;
                Properties.Settings.Default.ListTitleColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                MessageBox.Show("New color saved. Please restart the application for the changes to take effect.", "Color Scheme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void btn_CardTitleColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btn_CardTitleColor.BackColor = colorDialog1.Color;
                Properties.Settings.Default.CardTitleColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                MessageBox.Show("New color saved. \nPlease restart the application,in order for the changes to take effect.", "Color Scheme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void btn_CardDescriptionColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btn_CardDescriptionColor.BackColor = colorDialog1.Color;
                Properties.Settings.Default.CardDescriptionColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                MessageBox.Show("New color saved. \nPlease restart the application for the changes to take effect.", "Color Scheme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }
    }
}
