﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_EditCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EditCardForm));
            this.cb_IsFinished = new System.Windows.Forms.CheckBox();
            this.btn_CardAdd = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_Priority = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker_DateDue = new System.Windows.Forms.DateTimePicker();
            this.tb_Description = new System.Windows.Forms.TextBox();
            this.tb_CardTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_cardDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cb_IsFinished
            // 
            this.cb_IsFinished.AutoSize = true;
            this.cb_IsFinished.Location = new System.Drawing.Point(321, 138);
            this.cb_IsFinished.Name = "cb_IsFinished";
            this.cb_IsFinished.Size = new System.Drawing.Size(133, 21);
            this.cb_IsFinished.TabIndex = 19;
            this.cb_IsFinished.Text = "Mark as finished";
            this.cb_IsFinished.UseVisualStyleBackColor = true;
            // 
            // btn_CardAdd
            // 
            this.btn_CardAdd.AutoSize = true;
            this.btn_CardAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_CardAdd.Location = new System.Drawing.Point(13, 168);
            this.btn_CardAdd.Name = "btn_CardAdd";
            this.btn_CardAdd.Size = new System.Drawing.Size(115, 27);
            this.btn_CardAdd.TabIndex = 18;
            this.btn_CardAdd.Text = "Save";
            this.btn_CardAdd.UseVisualStyleBackColor = true;
            this.btn_CardAdd.Click += new System.EventHandler(this.btn_CardAdd_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(318, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Priority";
            // 
            // comboBox_Priority
            // 
            this.comboBox_Priority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Priority.FormattingEnabled = true;
            this.comboBox_Priority.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.comboBox_Priority.Location = new System.Drawing.Point(321, 103);
            this.comboBox_Priority.Name = "comboBox_Priority";
            this.comboBox_Priority.Size = new System.Drawing.Size(200, 24);
            this.comboBox_Priority.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(318, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Due Date";
            // 
            // dateTimePicker_DateDue
            // 
            this.dateTimePicker_DateDue.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_DateDue.Location = new System.Drawing.Point(321, 29);
            this.dateTimePicker_DateDue.Name = "dateTimePicker_DateDue";
            this.dateTimePicker_DateDue.ShowCheckBox = true;
            this.dateTimePicker_DateDue.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker_DateDue.TabIndex = 14;
            // 
            // tb_Description
            // 
            this.tb_Description.Location = new System.Drawing.Point(15, 83);
            this.tb_Description.Multiline = true;
            this.tb_Description.Name = "tb_Description";
            this.tb_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_Description.Size = new System.Drawing.Size(284, 76);
            this.tb_Description.TabIndex = 13;
            // 
            // tb_CardTitle
            // 
            this.tb_CardTitle.Location = new System.Drawing.Point(13, 30);
            this.tb_CardTitle.Name = "tb_CardTitle";
            this.tb_CardTitle.Size = new System.Drawing.Size(286, 22);
            this.tb_CardTitle.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Title";
            // 
            // btn_cardDelete
            // 
            this.btn_cardDelete.AutoSize = true;
            this.btn_cardDelete.Location = new System.Drawing.Point(184, 168);
            this.btn_cardDelete.Name = "btn_cardDelete";
            this.btn_cardDelete.Size = new System.Drawing.Size(115, 27);
            this.btn_cardDelete.TabIndex = 20;
            this.btn_cardDelete.Text = "❌ Delete to do";
            this.btn_cardDelete.UseVisualStyleBackColor = true;
            this.btn_cardDelete.Click += new System.EventHandler(this.btn_cardDelete_Click);
            // 
            // frm_EditCardForm
            // 
            this.AcceptButton = this.btn_CardAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(537, 213);
            this.Controls.Add(this.btn_cardDelete);
            this.Controls.Add(this.cb_IsFinished);
            this.Controls.Add(this.btn_CardAdd);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox_Priority);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker_DateDue);
            this.Controls.Add(this.tb_Description);
            this.Controls.Add(this.tb_CardTitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_EditCardForm";
            this.Text = "Edit to do";
            this.Load += new System.EventHandler(this.frm_EditCardForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cb_IsFinished;
        private System.Windows.Forms.Button btn_CardAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_Priority;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker_DateDue;
        private System.Windows.Forms.TextBox tb_Description;
        private System.Windows.Forms.TextBox tb_CardTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_cardDelete;
    }
}