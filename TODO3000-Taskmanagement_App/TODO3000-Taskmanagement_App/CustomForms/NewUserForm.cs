﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_NewUserForm : Form
    {
        public User selectedUser;
        public frm_NewUserForm()
        {
            InitializeComponent();
        }

        private void btn_CreateUser_Click(object sender, EventArgs e)
        {
            if (!(string.IsNullOrEmpty(tb_FirstName.Text) && string.IsNullOrEmpty(tb_LastName.Text)))
            {
                User newUser = new User();
                newUser.FirstName = tb_FirstName.Text;
                newUser.LastName = tb_LastName.Text;

                // shrani uporabnika v bazo
                newUser.Persist();

                //nastavimo na novega uporabnika, Form1 bo tega uporabnika prevzela
                selectedUser = newUser;

                // shrani UserID v Properties za naslednjič, ko se program odpre
                Properties.Settings.Default.LastUserID = selectedUser.UserID;
                Properties.Settings.Default.Save();


                // sporočimo "OK", da se forma zapre
                this.DialogResult = DialogResult.OK;
            }
        }

        private void frm_NewUserForm_Load(object sender, EventArgs e)
        {

        }
    }
}
