﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class UserSelectForm : Form
    {
        public User selectedUser;
        public UserSelectForm()
        {
            InitializeComponent();

            //napolni combobox z uporabniki 
            List<User> userList = User.GetAllUsers();

            comboBox1_Users.DataSource = userList;
            comboBox1_Users.DisplayMember = "FullName";
            comboBox1_Users.ValueMember = "UserID";

            //nastavi na trenutnega uporabnika (shranjenega v properties)
            comboBox1_Users.SelectedValue = Properties.Settings.Default.LastUserID;
        }

        private void btn_UserDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this User?", "Delete User",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // zbriši uporabnika ki je izbran
                selectedUser.Delete();

                //TODO kaj se zgodi če je ta uporabnik odprt
                
            }
            this.Close();
        }

        private void btn_SaveUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tb_FirstName.Text) &&
                !string.IsNullOrEmpty(tb_LastName.Text))
            {
                selectedUser = (User)comboBox1_Users.SelectedItem;
                selectedUser.FirstName = tb_FirstName.Text;
                selectedUser.LastName = tb_LastName.Text;

                //persist user -  shrani spremembe v izbranega uporabnika
                selectedUser.Persist();

                // sporočimo 
                MessageBox.Show("Changes are saved", "User profile", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //napolni combobox z uporabniki 
                List<User> userList = User.GetAllUsers();

                comboBox1_Users.DataSource = userList;
                comboBox1_Users.DisplayMember = "FullName";
                comboBox1_Users.ValueMember = "UserID";

                selectedUser = (User)comboBox1_Users.SelectedItem;
                tb_FirstName.Text = selectedUser.FirstName;
                tb_LastName.Text = selectedUser.LastName;

            }
            else if (string.IsNullOrEmpty(tb_FirstName.Text))
            {
                errorProvider1.SetError(tb_FirstName, "Please enter a valid first name");
            }
            else if (string.IsNullOrEmpty(tb_LastName.Text))
            {
                errorProvider1.SetError(tb_LastName, "Please enter a valid last name");
            }
            
        }

        private void UserSelectForm_Load(object sender, EventArgs e)
        {
            // prikaži trenutnega userja v comboboxu
            if (comboBox1_Users.Text != "")
            {
                var user = new User();
                user = (User)comboBox1_Users.SelectedItem;

                tb_FirstName.Text = user.FirstName;
                tb_LastName.Text = user.LastName;

            }
        }

        private void comboBox1_Users_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //prikaži ob spremembi combobxa podatke v textboxih
            selectedUser = (User)comboBox1_Users.SelectedItem;

            tb_FirstName.Text = selectedUser.FirstName;
            tb_LastName.Text = selectedUser.LastName;
        }

        private void btnSelectProject_Click(object sender, EventArgs e)
        {
            selectedUser = (User)comboBox1_Users.SelectedItem;

            // shrani UserID v Properties za naslednjič, ko se program odpre
            Properties.Settings.Default.LastUserID = selectedUser.UserID;
            Properties.Settings.Default.Save();

            this.DialogResult = DialogResult.OK;
        }
    }
}
