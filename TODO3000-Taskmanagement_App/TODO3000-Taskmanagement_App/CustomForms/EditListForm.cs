﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_EditListForm : Form
    {
        public CardList CardList; // sem shranimo podatke o CardListu, ki ga vrnemo kontroli
        public frm_EditListForm()
        {
            InitializeComponent();
        }
        public frm_EditListForm(CardList cList)
        {
            InitializeComponent();
            CardList = cList;
            tb_ListTitle.Text = CardList.Title;
        }

        private void frm_EditListForm_Load(object sender, EventArgs e)
        {

        }

        private void btn_CardAdd_Click(object sender, EventArgs e)
        {
            // shrani spremembe v lastnostih seznama 
            if (!string.IsNullOrEmpty(tb_ListTitle.Text))
            {
                CardList.Title = tb_ListTitle.Text;
                CardList.Persist();
            }
        }

        private void btn_cardDelete_Click(object sender, EventArgs e)
        {
            // vprašaj za potrditev pri brisanju seznama
            if (MessageBox.Show("Are you sure you want to delete this list?", "Delete list",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // zbriši cardlist
                CardList.DeleteListandTodos(); 
                
            }
            DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
