﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class ProjectSelectForm : Form
    {
        List<Project> projectList;
        Project selectedProject;
        frm_TodoMain formMain;
        User currentUser;
        public ProjectSelectForm()
        {
            InitializeComponent();
        }

        public ProjectSelectForm(User usr, frm_TodoMain frm)
        {
            InitializeComponent();

            currentUser = usr;
            projectList = Project.GetAllProjectsList(usr.UserID);
            formMain = frm;
        }



        private void ProjectSelectForm_Load(object sender, EventArgs e)
        {
            // Binding Listbox to Combobox
            //https://stackoverflow.com/questions/2675067/binding-listbox-to-listobject-in-winforms

            listBox_Projects.DataSource = projectList;
            listBox_Projects.DisplayMember = "Title";
            listBox_Projects.ValueMember = "ProjectID";
            

        }

        private void SelectProject_click(object sender, EventArgs e)
        {
            int selectedProjectID = Convert.ToInt32(listBox_Projects.SelectedValue);

            selectedProject = (Project)listBox_Projects.SelectedItem;
            // pošlji izbrani projekt v glavno formo, da se dodajo kartice in seznami
            // preberi iz baze podatke
            formMain.ShowProjectOnCurrentTab(selectedProject);
            formMain.ShowAllProjectsOnOverviewTab();

            DialogResult = DialogResult.OK;
            // sproži tabswitch event
            formMain.SwitchTab(1);
        }

        private void btn_DeleteProject_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this Board?", "Delete Board",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                selectedProject = (Project)listBox_Projects.SelectedItem;
                selectedProject.DeleteProjectandSubItems();


                // ponovno preberi seznam
                projectList = Project.GetAllProjectsList(currentUser.UserID);
                listBox_Projects.DataSource = projectList;
                listBox_Projects.DisplayMember = "Title";
                listBox_Projects.ValueMember = "ProjectID";
            }
        }

        private void listBox_Projects_SelectedIndexChanged(object sender, EventArgs e)
        {

            // ob spremembi listboxa prikaži podatke v textboxih
            selectedProject = (Project)listBox_Projects.SelectedItem;
            tb_ProjectTitle.Text = selectedProject.Title;
        }

        private void btn_Rename_Click(object sender, EventArgs e)
        {
            selectedProject = (Project)listBox_Projects.SelectedItem;
            if (!string.IsNullOrEmpty(tb_ProjectTitle.Text))
            {
                selectedProject.Title = tb_ProjectTitle.Text;
                selectedProject.Persist();

                // ponovno preberi seznam
                projectList = Project.GetAllProjectsList(currentUser.UserID);
                listBox_Projects.DataSource = projectList;
                listBox_Projects.DisplayMember = "Title";
                listBox_Projects.ValueMember = "ProjectID";
            }
            else
            {
                errorProvider1.SetError(tb_ProjectTitle, "Please enter a Title");
            }
        }
    }
}
