﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_NewListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_NewListForm));
            this.tb_ListTitle = new System.Windows.Forms.TextBox();
            this.lbl_ListTitle = new System.Windows.Forms.Label();
            this.btn_AddList = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_ListTitle
            // 
            this.tb_ListTitle.Location = new System.Drawing.Point(12, 33);
            this.tb_ListTitle.Name = "tb_ListTitle";
            this.tb_ListTitle.Size = new System.Drawing.Size(251, 22);
            this.tb_ListTitle.TabIndex = 0;
            this.tb_ListTitle.TextChanged += new System.EventHandler(this.tb_ListTitle_TextChanged);
            // 
            // lbl_ListTitle
            // 
            this.lbl_ListTitle.AutoSize = true;
            this.lbl_ListTitle.Location = new System.Drawing.Point(12, 13);
            this.lbl_ListTitle.Name = "lbl_ListTitle";
            this.lbl_ListTitle.Size = new System.Drawing.Size(93, 17);
            this.lbl_ListTitle.TabIndex = 1;
            this.lbl_ListTitle.Text = "Enter list title:";
            this.lbl_ListTitle.Click += new System.EventHandler(this.lbl_ListTitle_Click);
            // 
            // btn_AddList
            // 
            this.btn_AddList.Location = new System.Drawing.Point(12, 61);
            this.btn_AddList.Name = "btn_AddList";
            this.btn_AddList.Size = new System.Drawing.Size(75, 29);
            this.btn_AddList.TabIndex = 2;
            this.btn_AddList.Text = "Add list";
            this.btn_AddList.UseVisualStyleBackColor = true;
            this.btn_AddList.Click += new System.EventHandler(this.btn_AddList_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frm_NewListForm
            // 
            this.AcceptButton = this.btn_AddList;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(299, 104);
            this.Controls.Add(this.btn_AddList);
            this.Controls.Add(this.lbl_ListTitle);
            this.Controls.Add(this.tb_ListTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_NewListForm";
            this.Text = "Add list";
            this.Load += new System.EventHandler(this.frm_NewListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_ListTitle;
        private System.Windows.Forms.Label lbl_ListTitle;
        private System.Windows.Forms.Button btn_AddList;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}