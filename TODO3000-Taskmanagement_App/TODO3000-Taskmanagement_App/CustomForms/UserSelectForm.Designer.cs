﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class UserSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserSelectForm));
            this.lbl_SelectUser = new System.Windows.Forms.Label();
            this.btnSelectProject = new System.Windows.Forms.Button();
            this.tb_FirstName = new System.Windows.Forms.TextBox();
            this.tb_LastName = new System.Windows.Forms.TextBox();
            this.lbl_Ime = new System.Windows.Forms.Label();
            this.lbl_Priimek = new System.Windows.Forms.Label();
            this.comboBox1_Users = new System.Windows.Forms.ComboBox();
            this.lbl_Separator = new System.Windows.Forms.Label();
            this.lbl_ModifyUser = new System.Windows.Forms.Label();
            this.btn_SaveUser = new System.Windows.Forms.Button();
            this.btn_UserDelete = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_SelectUser
            // 
            this.lbl_SelectUser.AutoSize = true;
            this.lbl_SelectUser.Location = new System.Drawing.Point(23, 25);
            this.lbl_SelectUser.Name = "lbl_SelectUser";
            this.lbl_SelectUser.Size = new System.Drawing.Size(82, 17);
            this.lbl_SelectUser.TabIndex = 5;
            this.lbl_SelectUser.Text = "Switch User";
            // 
            // btnSelectProject
            // 
            this.btnSelectProject.AutoSize = true;
            this.btnSelectProject.Location = new System.Drawing.Point(26, 125);
            this.btnSelectProject.Name = "btnSelectProject";
            this.btnSelectProject.Size = new System.Drawing.Size(123, 34);
            this.btnSelectProject.TabIndex = 4;
            this.btnSelectProject.Text = "Switch User";
            this.btnSelectProject.UseVisualStyleBackColor = true;
            this.btnSelectProject.Click += new System.EventHandler(this.btnSelectProject_Click);
            // 
            // tb_FirstName
            // 
            this.tb_FirstName.Location = new System.Drawing.Point(387, 49);
            this.tb_FirstName.Name = "tb_FirstName";
            this.tb_FirstName.Size = new System.Drawing.Size(128, 22);
            this.tb_FirstName.TabIndex = 6;
            // 
            // tb_LastName
            // 
            this.tb_LastName.Location = new System.Drawing.Point(387, 77);
            this.tb_LastName.Name = "tb_LastName";
            this.tb_LastName.Size = new System.Drawing.Size(128, 22);
            this.tb_LastName.TabIndex = 7;
            // 
            // lbl_Ime
            // 
            this.lbl_Ime.AutoSize = true;
            this.lbl_Ime.Location = new System.Drawing.Point(274, 52);
            this.lbl_Ime.Name = "lbl_Ime";
            this.lbl_Ime.Size = new System.Drawing.Size(76, 17);
            this.lbl_Ime.TabIndex = 8;
            this.lbl_Ime.Text = "First Name";
            // 
            // lbl_Priimek
            // 
            this.lbl_Priimek.AutoSize = true;
            this.lbl_Priimek.Location = new System.Drawing.Point(274, 78);
            this.lbl_Priimek.Name = "lbl_Priimek";
            this.lbl_Priimek.Size = new System.Drawing.Size(76, 17);
            this.lbl_Priimek.TabIndex = 9;
            this.lbl_Priimek.Text = "Last Name";
            // 
            // comboBox1_Users
            // 
            this.comboBox1_Users.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1_Users.FormattingEnabled = true;
            this.comboBox1_Users.Location = new System.Drawing.Point(26, 52);
            this.comboBox1_Users.Name = "comboBox1_Users";
            this.comboBox1_Users.Size = new System.Drawing.Size(200, 24);
            this.comboBox1_Users.TabIndex = 10;
            this.comboBox1_Users.SelectionChangeCommitted += new System.EventHandler(this.comboBox1_Users_SelectionChangeCommitted);
            // 
            // lbl_Separator
            // 
            this.lbl_Separator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_Separator.Location = new System.Drawing.Point(255, 19);
            this.lbl_Separator.Name = "lbl_Separator";
            this.lbl_Separator.Size = new System.Drawing.Size(1, 150);
            this.lbl_Separator.TabIndex = 11;
            // 
            // lbl_ModifyUser
            // 
            this.lbl_ModifyUser.AutoSize = true;
            this.lbl_ModifyUser.Location = new System.Drawing.Point(384, 25);
            this.lbl_ModifyUser.Name = "lbl_ModifyUser";
            this.lbl_ModifyUser.Size = new System.Drawing.Size(101, 17);
            this.lbl_ModifyUser.TabIndex = 12;
            this.lbl_ModifyUser.Text = "Change Profile";
            // 
            // btn_SaveUser
            // 
            this.btn_SaveUser.AutoSize = true;
            this.btn_SaveUser.Location = new System.Drawing.Point(271, 129);
            this.btn_SaveUser.Name = "btn_SaveUser";
            this.btn_SaveUser.Size = new System.Drawing.Size(108, 30);
            this.btn_SaveUser.TabIndex = 13;
            this.btn_SaveUser.Text = "Save changes";
            this.btn_SaveUser.UseVisualStyleBackColor = true;
            this.btn_SaveUser.Click += new System.EventHandler(this.btn_SaveUser_Click);
            // 
            // btn_UserDelete
            // 
            this.btn_UserDelete.AutoSize = true;
            this.btn_UserDelete.Location = new System.Drawing.Point(387, 129);
            this.btn_UserDelete.Name = "btn_UserDelete";
            this.btn_UserDelete.Size = new System.Drawing.Size(128, 30);
            this.btn_UserDelete.TabIndex = 14;
            this.btn_UserDelete.Text = "❌Delete User";
            this.btn_UserDelete.UseVisualStyleBackColor = true;
            this.btn_UserDelete.Visible = false;
            this.btn_UserDelete.Click += new System.EventHandler(this.btn_UserDelete_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // UserSelectForm
            // 
            this.AcceptButton = this.btnSelectProject;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 176);
            this.Controls.Add(this.btn_UserDelete);
            this.Controls.Add(this.btn_SaveUser);
            this.Controls.Add(this.lbl_ModifyUser);
            this.Controls.Add(this.lbl_Separator);
            this.Controls.Add(this.comboBox1_Users);
            this.Controls.Add(this.lbl_Priimek);
            this.Controls.Add(this.lbl_Ime);
            this.Controls.Add(this.tb_LastName);
            this.Controls.Add(this.tb_FirstName);
            this.Controls.Add(this.lbl_SelectUser);
            this.Controls.Add(this.btnSelectProject);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserSelectForm";
            this.Text = "Switch or Modify a User";
            this.Load += new System.EventHandler(this.UserSelectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_SelectUser;
        private System.Windows.Forms.Button btnSelectProject;
        private System.Windows.Forms.TextBox tb_FirstName;
        private System.Windows.Forms.TextBox tb_LastName;
        private System.Windows.Forms.Label lbl_Ime;
        private System.Windows.Forms.Label lbl_Priimek;
        private System.Windows.Forms.ComboBox comboBox1_Users;
        private System.Windows.Forms.Label lbl_Separator;
        private System.Windows.Forms.Label lbl_ModifyUser;
        private System.Windows.Forms.Button btn_SaveUser;
        private System.Windows.Forms.Button btn_UserDelete;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}