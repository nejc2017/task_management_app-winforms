﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class frm_SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_SearchForm));
            this.dataGridView_SearchResults = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbl_TitleSearch = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SearchResults)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_SearchResults
            // 
            this.dataGridView_SearchResults.AllowUserToAddRows = false;
            this.dataGridView_SearchResults.AllowUserToDeleteRows = false;
            this.dataGridView_SearchResults.AllowUserToOrderColumns = true;
            this.dataGridView_SearchResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_SearchResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_SearchResults.Location = new System.Drawing.Point(12, 47);
            this.dataGridView_SearchResults.Name = "dataGridView_SearchResults";
            this.dataGridView_SearchResults.ReadOnly = true;
            this.dataGridView_SearchResults.RowHeadersWidth = 51;
            this.dataGridView_SearchResults.RowTemplate.Height = 24;
            this.dataGridView_SearchResults.Size = new System.Drawing.Size(999, 303);
            this.dataGridView_SearchResults.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(212, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(219, 22);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // lbl_TitleSearch
            // 
            this.lbl_TitleSearch.AutoSize = true;
            this.lbl_TitleSearch.Location = new System.Drawing.Point(13, 12);
            this.lbl_TitleSearch.Name = "lbl_TitleSearch";
            this.lbl_TitleSearch.Size = new System.Drawing.Size(162, 17);
            this.lbl_TitleSearch.TabIndex = 2;
            this.lbl_TitleSearch.Text = "Search through TO DOs";
            // 
            // btn_Clear
            // 
            this.btn_Clear.AutoSize = true;
            this.btn_Clear.Location = new System.Drawing.Point(450, 10);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 27);
            this.btn_Clear.TabIndex = 3;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // frm_SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 362);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.lbl_TitleSearch);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView_SearchResults);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "frm_SearchForm";
            this.Text = "Find a to do in current Board";
            this.Load += new System.EventHandler(this.frm_SearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SearchResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_SearchResults;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbl_TitleSearch;
        private System.Windows.Forms.Button btn_Clear;
    }
}