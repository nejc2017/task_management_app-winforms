﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{

    public partial class NewProjectForm : Form
    {
        public User SelectedUser { get; set; }
        public Project NewProject { get; set; }
        public NewProjectForm()
        {
            InitializeComponent();
        }
        public NewProjectForm(User user)
        {
            InitializeComponent();
            SelectedUser = user;

        }

        private void btn_AddBoard_Click(object sender, EventArgs e)
        {
            // Ustvari nov projekt/board. Glavna forma ga bo prevzela kot currentProject
            Project newProject = new Project(SelectedUser);
            newProject.Title = tb_ListTitle.Text;
            newProject.Persist();
            NewProject = newProject;

            this.DialogResult = DialogResult.OK;
        }

        private void NewProjectForm_Load(object sender, EventArgs e)
        {

        }
    }
}
