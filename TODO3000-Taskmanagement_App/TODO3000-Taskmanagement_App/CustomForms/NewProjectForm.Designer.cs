﻿namespace TODO3000_Taskmanagement_App.CustomForms
{
    partial class NewProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewProjectForm));
            this.btn_AddBoard = new System.Windows.Forms.Button();
            this.lbl_ProjectTitle = new System.Windows.Forms.Label();
            this.tb_ListTitle = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_AddBoard
            // 
            this.btn_AddBoard.AutoSize = true;
            this.btn_AddBoard.Location = new System.Drawing.Point(12, 59);
            this.btn_AddBoard.Name = "btn_AddBoard";
            this.btn_AddBoard.Size = new System.Drawing.Size(119, 27);
            this.btn_AddBoard.TabIndex = 5;
            this.btn_AddBoard.Text = "Create board";
            this.btn_AddBoard.UseVisualStyleBackColor = true;
            this.btn_AddBoard.Click += new System.EventHandler(this.btn_AddBoard_Click);
            // 
            // lbl_ProjectTitle
            // 
            this.lbl_ProjectTitle.AutoSize = true;
            this.lbl_ProjectTitle.Location = new System.Drawing.Point(12, 9);
            this.lbl_ProjectTitle.Name = "lbl_ProjectTitle";
            this.lbl_ProjectTitle.Size = new System.Drawing.Size(210, 17);
            this.lbl_ProjectTitle.TabIndex = 4;
            this.lbl_ProjectTitle.Text = "Enter the title of your new board";
            // 
            // tb_ListTitle
            // 
            this.tb_ListTitle.Location = new System.Drawing.Point(12, 29);
            this.tb_ListTitle.Name = "tb_ListTitle";
            this.tb_ListTitle.Size = new System.Drawing.Size(210, 22);
            this.tb_ListTitle.TabIndex = 3;
            // 
            // NewProjectForm
            // 
            this.AcceptButton = this.btn_AddBoard;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 96);
            this.Controls.Add(this.btn_AddBoard);
            this.Controls.Add(this.lbl_ProjectTitle);
            this.Controls.Add(this.tb_ListTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewProjectForm";
            this.Text = "New Board";
            this.Load += new System.EventHandler(this.NewProjectForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_AddBoard;
        private System.Windows.Forms.Label lbl_ProjectTitle;
        private System.Windows.Forms.TextBox tb_ListTitle;
    }
}