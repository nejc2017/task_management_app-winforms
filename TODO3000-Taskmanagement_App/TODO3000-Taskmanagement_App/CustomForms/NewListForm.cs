﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomForms
{
    public partial class frm_NewListForm : Form
    {
        public Project CurrentProject { get; set; }
        public frm_NewListForm()
        {
            InitializeComponent();
        }

        public frm_NewListForm(Project proj)
        {
            InitializeComponent();
            CurrentProject = proj;
        }

        private void btn_AddList_Click(object sender, EventArgs e)
        {
            //pošljemo trenutni projekt zaradi IDja na katerega bo vezan CardList
            if (string.IsNullOrEmpty(tb_ListTitle.Text))
            {
                errorProvider1.SetError(tb_ListTitle, "Please enter the list title");
            }
            else
            {
                CardList nCardList = new CardList(CurrentProject);
                nCardList.Title = tb_ListTitle.Text;
                nCardList.Persist();
                DialogResult = DialogResult.OK;
            }
        }

        private void lbl_ListTitle_Click(object sender, EventArgs e)
        {

        }

        private void tb_ListTitle_TextChanged(object sender, EventArgs e)
        {

        }

        private void frm_NewListForm_Load(object sender, EventArgs e)
        {

        }
    }
}
