﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App
{

    class ColorRandomizer
    {
        static List<KnownColor> selectedColors = new List<KnownColor>();
        static Random rnd = new Random();
        static int previous;
        static int current = 0;
        ColorRandomizer()
        {
        }

        public static KnownColor RandomColor()
        {
            //določi barve
            selectedColors.AddRange(new List<KnownColor>() { KnownColor.LightBlue, 
            KnownColor.LightGray, KnownColor.LightSteelBlue, KnownColor.LightSlateGray });
            //KnownColor randomColor = new KnownColor();

            //current = rnd.Next(0, selectedColors.Count);
            //if (previous == current)
            //{
            //    current = rnd.Next(0, selectedColors.Count);
            //}
            //randomColor = selectedColors[current];
            //previous = current;

            if (current >= selectedColors.Count)
            {
                current = 0;
            }
            else
            {
                current++;

            }
            var currentColor = selectedColors[current];
            return currentColor;
        }

    }
   
}
