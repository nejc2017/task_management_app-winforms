﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App
{
    class ExportBoard
    {
        public Project selectedProject { get; set; }
        public List<string> formattedText { get; set; }
        // get project
        public ExportBoard (Project exportProject)
        {
            selectedProject = exportProject;
            formattedText = new List<string>();

        }

        public List<string> FormatToText()
        {
            /* Primer prikaza:
            
                # Title of project

                ## List Title
                [ ] Card -x if done
                    Description
                [x] Card is done

                ## List 2
                [ ] To Do 2
            
            */
            //zaiši naslov in podčrtaj
            int titleLength = selectedProject.Title.Length;
            string titleUnderline = new string('=', titleLength);
            formattedText.Add(selectedProject.Title + Environment.NewLine + 
                titleUnderline + Environment.NewLine);

            // seznami
            //selectedProject.CardList.First(); 


            foreach (var c_list in selectedProject.CardList)
            {
                int todoTitleLength = c_list.Title.Length;
                string todoTitleUnderline = new string('-', todoTitleLength);
                formattedText.Add(Environment.NewLine +  c_list.Title + 
                    Environment.NewLine + todoTitleUnderline);


                // todoji
                foreach (ToDo todoItem in c_list.TodoList)
                {
                    // je todo dokončan ali še ne
                    if (todoItem.DateFinished > DateTime.MinValue)
                    {
                        formattedText.Add("[x] " + todoItem.Title);

                    }
                    else
                    {
                        formattedText.Add("[ ] " + todoItem.Title);
                    }

                    // opis
                    if (!string.IsNullOrEmpty(todoItem.Description))
                    {
                        formattedText.Add("    (" + todoItem.Description + ")");
                    }
                }
            }

            return formattedText;
        }
    }
}
