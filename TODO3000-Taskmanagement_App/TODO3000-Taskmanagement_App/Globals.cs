﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App
{

    // razred globalnih spremenljivk, 
    // do katerega dostopamo iz drugih razredov

    public class Globals
    {
        //SQLite
        public static string SqlConnection = @"Data Source=db\todo3000_db.db;Version=3;";

        //Options
        public static Color ListTitleColor = SystemColors.HotTrack;
        public static Color CardTitleColor = Color.Black;
        public static Color ListDescriptionColor = SystemColors.GrayText;

    }
}
