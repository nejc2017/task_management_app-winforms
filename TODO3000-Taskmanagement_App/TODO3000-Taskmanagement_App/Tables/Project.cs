﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App.Tables
{
    public class Project
    {
        public int ProjectID { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserID { get; set; }
        public List<Project> ProjectList { get; set; }
        public List<CardList> CardList { get; set; }


        public Project(int projectId)
        {
            // za listbox v Select Project Form
            UserID = projectId;
        }

        // potrebujemo uporabnika, ki bo lastnik projekta
        public Project(User user)
        {
            UserID = user.UserID;
        }
        public Project(DataRow dataRow)
        {
            ProjectID = Convert.ToInt32(dataRow["ProjectID"]);
            Title = (string)dataRow["Title"];
            //DateCraeted zaenkrat spustimo
            UserID = Convert.ToInt32(dataRow["UserID"]);
        }

        // Nov projekt oz shrani spremembe v obstoječem projekti
        // narejeno po TODO.cs
        public int Persist()
        {
            // preveri, če objekt ne obstaja (ID = 0), naredi novega
            if (this.ProjectID == 0)
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Insert into Projects 
                                        (Title, 
                                        DateCreated, 
                                        UserID)
                                        Values (@Title, @DateCreated, @UserID);
                                        SELECT last_insert_rowid() as int"; // vrni ProjectID

                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("DateCreated", this.DateCreated));
                        command.Parameters.Add(new SQLiteParameter("UserID", this.UserID));
                        object temp = command.ExecuteScalar();
                        this.ProjectID = Convert.ToInt32(temp);
                    }
                }
            }
            else // če  objekt že obstaja, ga posodobi
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Update Projects set
                                    Title = @Title, 
                                    DateCreated = @DateCreated, 
                                    UserID = @UserID
                                    WHERE ProjectID = @ProjectID";
                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("DateCreated", this.DateCreated));
                        command.Parameters.Add(new SQLiteParameter("UserID", this.UserID));

                        command.ExecuteNonQuery();
                    }
                }
            }
            // vrni ID (npr. če gre za nov objekt)
            return this.ProjectID;
        }


        //pridobi samo seznam projektov glede na uporabnika
        // statična metoda, ker koda ni odvisna od instacializiranega objekta
        public static List<Project> GetAllProjectsList(int userID)
        {
            // poišči vse projekte
            string cmd = @"Select * from Projects where UserID = @UserID";

            // tabela, ki jo bomo zapolnili z vnosi
            DataTable dataTable = new DataTable();


            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("UserID", userID));
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(dataTable);
                    }
                }
            }

            // kreiraj listo in napolni z Project objekti
            List<Project> p_list = new List<Project>();

            foreach (DataRow row in dataTable.Rows)
            {
                p_list.Add(new Project(row));
            }

            return p_list;
        }

        public Project GetProject()
        {
            // poišči projekt/board s specifičnim IDjem
            string cmd = @"Select * from Projects where ProjectID = @ProjectID";
            DataTable projectData = new DataTable();

            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                {

                    command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(projectData);
                    }
                }

            }

            // izberi prvo vrstico
            var receivedProject = new Project(projectData.Rows[0]);

            // preberi sezname
            receivedProject.GetAllListsByProject();

            return receivedProject;
        }


        //pridobi vse Cardliste ki spadajo pod za projekt
        public Project GetAllListsByProject()
        {
            string cmd = @"Select * from Lists where ProjectID = @ProjectID";
            var dataLists = new DataTable();

            using (SQLiteConnection conn = new SQLiteConnection(Globals.SqlConnection))
            {
                conn.Open();

                using (SQLiteCommand command = new SQLiteCommand(cmd, conn))
                {
                    command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));

                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(dataLists);
                    }
                }
            }

            //napolni listo s CardListi (stolpci)
            var p_cardList = new List<CardList>();
            foreach (DataRow row in dataLists.Rows)
            {
                p_cardList.Add(new CardList(row));
            }
            this.CardList = p_cardList;
            return this;
        }

        public void DeleteProjectandSubItems()
        {
            // pridobi vse sezname projekta (IDje)
            Project tempProject = this.GetAllListsByProject();
            List<ToDo> allTodosFromLists = new List<ToDo>();


            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                // odstrani vse sezname,  todoje in komentarje, ki so povezani s seznamom
                foreach (var listItem in tempProject.CardList)
                {
                    listItem.DeleteListandTodos();
                }

                // odstrani specifičen projekt
                string cmdList = @"Delete from Projects where ProjectID = @ProjectID";
                using (SQLiteCommand command = new SQLiteCommand(cmdList, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}