﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App.Tables
{


    public class ToDo
    {

        public int ToDoID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Position { get; set; }
        public DateTime DateDue { get; set; }
        public DateTime DateCreated { get; set; }
        public int Priority { get; set; }
        public DateTime DateFinished { get; set; }
        public int ListID { get; set; }

        // konstruktorji
        public ToDo()
        {
            DateCreated = DateTime.Now;
        }

        // uporaba pri metodi GetTodoByListID, ko preberemo celoten DataTable,
        // naredimo nov ToDo objekt in mapiramo vrednosti iz DataRow-a na Property ToDO-ja
        public ToDo(DataRow row)
        {
            ToDoID = Convert.ToInt32(row["ToDoID"]);
            Title = (string)row["Title"];
            ListID = Convert.ToInt32(row["ListID"]);
            Description = (string)row["Description"];
            Position = Convert.ToInt32(row["Position"]);
            Priority = Convert.ToInt32(row["Priority"]);
            if (row["DateDue"] != System.DBNull.Value)
            {
                DateDue = Convert.ToDateTime(row["DateDue"]);
            }
            else
            {
                DateDue = DateTime.MinValue;
            }
            DateCreated = Convert.ToDateTime(row["DateCreated"]);
            
            if (row["DateFinished"] != System.DBNull.Value)
            {
                DateFinished = Convert.ToDateTime(row["DateFinished"]);
            }
            else
            {
                DateDue = DateTime.MinValue;
            }
        }


        //  Shrani spremembe ali ustvari nov objekt
        //  in vrni ID TODOja
        public int Persist()
        {
            // preveri, če TODO objekt ne obstaja (ID = 0), naredi novega
            if (this.ToDoID == 0)
            {
                // Using statement in uporaba connection.close() https://stackoverflow.com/questions/26707451/do-i-need-to-close-connection-when-using-a-using-statement
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Insert into ToDos 
                                        (Title, 
                                        Description, 
                                        Position, 
                                        Priority,
                                        DateDue, 
                                        DateCreated, 
                                        DateFinished, 
                                        ListID)
                                        Values (@Title, @Description, @Position, @Priority, @DateDue, @DateCreated, @DateFinished, @ListID);
                                        SELECT last_insert_rowid() as int";


                    // TODO Viri&Literatura
                    // https://www.techonthenet.com/sqlite/functions/last_insert_rowid.php
                    // Link do povezave https://stackoverflow.com/a/62234132

                    //https://stackoverflow.com/questions/304543/does-sqlite-support-scope-identity
                    // Uporaba lstInsertRow ID https://stackoverflow.com/questions/4341178/getting-the-last-insert-id-with-sqlite-net-in-c-sharp
                    
                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("Description", this.Description));
                        command.Parameters.Add(new SQLiteParameter("Position", this.Position));
                        command.Parameters.Add(new SQLiteParameter("Priority", this.Priority));
                        command.Parameters.Add(new SQLiteParameter("DateDue", this.DateDue));
                        command.Parameters.Add(new SQLiteParameter("DateCreated", this.DateCreated));
                        command.Parameters.Add(new SQLiteParameter("DateFinished", this.DateFinished));
                        command.Parameters.Add(new SQLiteParameter("ListID", this.ListID));
                        //command.Parameters.Add(new SQLiteParameter("ToDoID", connection.LastInsertRowId));
                        object temp = command.ExecuteScalar();
                        this.ToDoID = Convert.ToInt32(temp);

                    }

                }
            }
            else // če TODO objekt že obstaja, ga posodobi
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Update ToDos set
                                    Title = @Title,
                                    Description = @Description,
                                    Position = @Position, 
                                    Priority = @Priority, 
                                    DateDue = @DateDue, 
                                    DateCreated = @DateCreated,
                                    DateFinished = @DateFinished, 
                                    ListID = @ListID
                                    WHERE ToDoId = @ToDoID";


                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("ToDoID", this.ToDoID));
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("Description", this.Description));
                        command.Parameters.Add(new SQLiteParameter("Position", this.Position));
                        command.Parameters.Add(new SQLiteParameter("Priority", this.Priority));
                        command.Parameters.Add(new SQLiteParameter("DateDue", this.DateDue));
                        command.Parameters.Add(new SQLiteParameter("DateCreated", this.DateCreated));
                        command.Parameters.Add(new SQLiteParameter("DateFinished", this.DateFinished));
                        command.Parameters.Add(new SQLiteParameter("ListID", this.ListID));

                        command.ExecuteNonQuery();

                    }
                }
            }
            // vrni ID (npr. če gre za nov objekt TODO)
            return this.ToDoID;
        }

        public void DeleteTodoandComments()
        {
            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                // odstrani vse komentarje, ki imajo za FK id od TODOja, ki ga bomo zbrisali
                string cmdComments = @"Delete from Comments where ToDoID = @ToDoID";
                using (SQLiteCommand command = new SQLiteCommand(cmdComments, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("ToDoID", this.ToDoID));
                    command.ExecuteNonQuery();
                }

                // odstrani specifičen todo
                string cmdTodos = @"Delete from ToDos where ToDoId = @ToDoID";
                using (SQLiteCommand command = new SQLiteCommand(cmdTodos, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("ToDoID", this.ToDoID));
                    command.ExecuteNonQuery();
                }

            }
        }

        // vrni seznam vseh TODOjev v projektu -  za search/find formo
        public static List<ToDo> GetAllTodosByProjectID (int projectID)
        {

            //string cmd = "Select * from Todo where Todo.ListID = @ListID";
            string cmd = @"Select * from ToDos
                            Inner Join Lists
                            On Todos.ListID = Lists.ListID
                            Where Lists.projectID = @projectID";

            DataTable data = new DataTable();
            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("projectID", projectID));
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(data);
                    }
                }

                connection.Close();
            }

            //naredi nov seznam todo-jev
            List<ToDo> t_list = new List<ToDo>();
            // za vsako vrstico kreiraj nov todo in ga dodaj v seznam todojev
            foreach (DataRow row in data.Rows)
            {
                t_list.Add(new ToDo(row));
            }
            return t_list;
        }
        public static List<ToDo> SearchTodosByProjectID(int projectID, string searchQuery)
        {

            string cmd = @"Select * from ToDos Inner Join Lists On Todos.ListID = Lists.ListID 
            Where Lists.projectID = @projectID And (ToDos.Title Like @searchQuery OR ToDos.Description Like @searchQuery)";

            DataTable data = new DataTable();
            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("projectID", projectID));
                    command.Parameters.Add(new SQLiteParameter("searchQuery", "%" + searchQuery + "%"));

                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(data);
                    }
                }

                connection.Close();
            }

            //naredi nov seznam todo-jev
            List<ToDo> t_list = new List<ToDo>();
            // za vsako vrstico kreiraj nov todo in ga dodaj v seznam todojev
            foreach (DataRow row in data.Rows)
            {
                t_list.Add(new ToDo(row));
            }
            return t_list;
        }


    }
}
