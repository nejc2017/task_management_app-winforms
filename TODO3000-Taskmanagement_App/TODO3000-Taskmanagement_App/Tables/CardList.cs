﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App.Tables
{

    public class CardList
    {
        public int ListID { get; set; }
        public int ProjectID { get; set; }
        public int Position { get; set; }
        public string Title { get; set; }

        public List<ToDo> TodoList { get; set; }


        // naredi nov prazen seznam
        public CardList(Project project)
        {
            ProjectID = project.ProjectID;
        }

        // pridobi todoje za obstoječ seznam
        public CardList(int id)
        {
            ListID = id;

            TodoList = GetTodoByListId(id);
        }
        

        // pri kreiranju projektov in ob prebiranju queryjev 
        public CardList(DataRow row)
        {
            ListID = Convert.ToInt32(row["ListID"]);
            Title = (string)row["Title"];
            ProjectID = Convert.ToInt32(row["ProjectID"]);
            Position = Convert.ToInt32(row["Position"]);

            //pridobi todoje
            TodoList = GetTodoByListId(ListID);

        }

        //ustvari nov seznam (CardList) ali shrani spremembe v obstoječem
        public int Persist()
        {
            // preveri, če seznam ne obstaja (ID = 0), naredi novega
            if (this.ListID == 0)
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Insert into Lists 
                                        (Title,  
                                        Position, 
                                        ProjectID)
                                        Values (@Title, @Position, @ProjectID);
                                        SELECT last_insert_rowid() as int";

                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("Position", this.Position));
                        command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));
                        command.Parameters.Add(new SQLiteParameter("ListID", this.ListID));
                        object temp = command.ExecuteScalar();
                        this.ListID = Convert.ToInt32(temp);

                    }

                }
            }
            else // če setnam (CardList) objekt že obstaja, ga posodobi in shrani spremembe
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Update Lists set
                                    Title = @Title,
                                    Position = @Position, 
                                    ProjectID = @ProjectID 
                                    WHERE ListID = @ListID";


                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("Title", this.Title));
                        command.Parameters.Add(new SQLiteParameter("Position", this.Position));
                        command.Parameters.Add(new SQLiteParameter("ProjectID", this.ProjectID));
                        command.Parameters.Add(new SQLiteParameter("ListID", this.ListID));
                        command.ExecuteNonQuery();

                    }
                }
            }
            // vrni ID (npr. če gre za nov objekt List)
            return this.ListID;
        }

        // CardList: pridobi vse TODOje za določen seznam
        public static List<ToDo> GetTodoByListId(int listid)
        {

            //string cmd = "Select * from Todo where Todo.ListID = @ListID";
            string cmd = @"Select * from Todos where ListID = @ListID";

            DataTable data = new DataTable();
            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("ListID", listid));
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        da.Fill(data);
                    }
                }

                connection.Close();
            }

            //naredi nov seznam todo-jev
            List<ToDo> t_list = new List<ToDo>();
            // za vsako vrstico kreiraj nov todo in ga dodaj v seznam todojev
            // s pomočjo konstruktorja ToDo(DataRow row) določimo vse lastnosti
            foreach (DataRow row in data.Rows)
            {
                t_list.Add(new ToDo(row));
            }
            return t_list;
        }

        public void DeleteListandTodos()
        {
            using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
            {
                connection.Open();

                // odstrani vse todoje, ki so povezani s seznamom
                foreach (var todoItem in TodoList)
                {
                    todoItem.DeleteTodoandComments();
                }

                // odstrani seznam
                string cmdList = @"Delete from Lists where ListID = @ListID";
                using (SQLiteCommand command = new SQLiteCommand(cmdList, connection))
                {
                    command.Parameters.Add(new SQLiteParameter("ListID", this.ListID));
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
