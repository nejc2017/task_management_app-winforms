﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App.Tables
{
    public class User
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        
        // za combobox v SwitchUser formi
        public string FullName 
        {
            get { return FirstName + " " + LastName; }
        }

        public User()
        {

        }
        public User(DataRow row)
        {
            this.UserID = Convert.ToInt32(row["UserID"]);
            this.FirstName = (string)row["FirstName"];
            this.LastName = (string)row["Lastname"];
        }

        // Crate & Update
        // shrani ali ustvari novega uporabnika
        public int Persist()
        {
            // preveri, če User ne obstaja (ID = 0), naredi novega
            // narejeno podobno kot ToDo.cs
            if (this.UserID == 0)
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Insert into Users 
                                        (FirstName, 
                                        LastName)
                                        Values (@FirstName, @LastName);
                                        SELECT last_insert_rowid() as int";


                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("FirstName", this.FirstName));
                        command.Parameters.Add(new SQLiteParameter("LastName", this.LastName));
                        object temp = command.ExecuteScalar();
                        this.UserID = Convert.ToInt32(temp);

                    }

                }
            }
            else // če User objekt že obstaja, ga posodobi (npr pri spremembi imena, priimka)
            {
                using (SQLiteConnection connection = new SQLiteConnection(Globals.SqlConnection))
                {
                    connection.Open();

                    string cmd = @"Update Users set
                                    FirstName = @FirstName,
                                    LastName = @LastName
                                    WHERE UserID = @UserID";


                    using (SQLiteCommand command = new SQLiteCommand(cmd, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("UserID", this.UserID));
                        command.Parameters.Add(new SQLiteParameter("FirstName", this.FirstName));
                        command.Parameters.Add(new SQLiteParameter("LastName", this.LastName));
                        command.ExecuteNonQuery();
                    }
                }
            }
            // vrni ID (npr. če gre za nov objekt)
            return this.UserID;
        }

        // Zbriši uporabnika, glede na ID
        public void Delete()
        {
            using (SQLiteConnection conn = new SQLiteConnection(Globals.SqlConnection))
            {
                conn.Open();

                string cmd = @"Delete from Users where UserID = @UserID";
                using (SQLiteCommand command = new SQLiteCommand(cmd, conn))
                {
                    command.Parameters.Add(new SQLiteParameter("UserID", this.UserID));
                    command.ExecuteNonQuery();
                }

            }
        }

        // Pridobi informacije o vseh uporabnikih

        public static User GetUser(int UserID)
        {
            DataTable dataTableUser = new DataTable();


            //Poišči v tabeli
            using (SQLiteConnection conn = new SQLiteConnection(Globals.SqlConnection))
            {

                conn.Open();

                string cmd = @"Select * from Users where UserID = @UserID";
                using (SQLiteCommand command = new SQLiteCommand(cmd, conn))
                {
                    command.Parameters.Add(new SQLiteParameter("UserID", UserID));

                    // sproži ukaz z DataAdapterjem
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        //napolni DataTable z DataAdapterjem
                        da.Fill(dataTableUser);

                    }
                }

            }
            //pridobi podatke iz dataTable
            DataRow dataRow = dataTableUser.Rows[0];

            // ustvari uporabnika s konstruktorjem
            var user = new User(dataRow);
            return user;
        }


        public static List<User> GetAllUsers()
        {
            DataTable dataUsers = new DataTable();


            //Poišči v tabeli
            using (SQLiteConnection conn = new SQLiteConnection(Globals.SqlConnection))
            {

                conn.Open();

                string cmd = @"Select * from Users";
                using (SQLiteCommand command = new SQLiteCommand(cmd, conn))
                {
                    //command.Parameters.Add(new SQLiteParameter("UserID", UserID));

                    // sproži ukaz z DataAdapterjem
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter(command))
                    {
                        //napolni DataTable z DataAdapterjem
                        da.Fill(dataUsers);

                    }
                }

            }

            // seznam uporabnikov, ki ga bomo napolnili iz dataRowa
            var userList = new List<User>();
            // napolni seznam z uporabniki
            foreach (DataRow row in dataUsers.Rows)
            {

                var user = new User(row);
                userList.Add(user);
            }

            //vrni seznam
            return userList;
        }

    }
}