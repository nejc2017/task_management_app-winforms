﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODO3000_Taskmanagement_App.Tables
{
    class Comment
    {
        public int CommentID { get; set; }
        public string Description { get; set; }
        public int Position { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserID { get; set; }
        public int ToDoID { get; set; }
    }
}
