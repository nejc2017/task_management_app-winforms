﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.CustomControl;
using TODO3000_Taskmanagement_App.CustomForms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App
{
    public partial class frm_TodoMain : Form
    {
        // trenutno uporabljen uporabnik
        User currentUser;
        public Project currentProject;

        public frm_TodoMain()
        {
            InitializeComponent();

            //pozicija okna
            this.StartPosition = FormStartPosition.CenterScreen;
        }


        private void toolStripButton_ListAdd_Click(object sender, EventArgs e)
        {
            // dodaj nov seznam na tabpage
            
     

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            CustomForms.frm_AboutForm frm_about = new CustomForms.frm_AboutForm();
            frm_about.StartPosition = FormStartPosition.CenterParent;
            frm_about.ShowDialog();

        }


        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (ProjectSelectForm projectSelectForm = new ProjectSelectForm(currentUser, this)) 
            {
                if (projectSelectForm.ShowDialog() == DialogResult.OK)
                {
                    //TODO izbran projekt prikaži

                }
            }
        }

        private void advancedSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentProject != null)
            {
                using (frm_SearchForm searchForm = new frm_SearchForm(currentProject))
                {
                    searchForm.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Currently no Board is selected. Please select a " +
                "board on the main tab or by clicking File > Open Board.", "Find To Do",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            SwitchTab(1);

        }

        // zagon programa
        private void frm_TodoMain_Load(object sender, EventArgs e)
        {

            // zadnji shranjen uporabnik
            int lastUserID = Properties.Settings.Default.LastUserID;
            
            // poišči uporabnika
            currentUser = User.GetUser(lastUserID);


            if (currentUser == null) // öe userja ne najde
            {
                

                using (UserSelectForm usf = new UserSelectForm())
                {
                    if (usf.ShowDialog() == DialogResult.OK)
                    {
                        currentUser = usf.selectedUser;
                    }
                }

            }


            // prikaži grid projektov na Oveview Tabu
            ShowAllProjectsOnOverviewTab();

            //lbl_BoardOverview.Text = "Boards from " + currentUser.FullName;
            
            ////TODO get projects and ...
            //var projectsList = Project.GetAllProjectsList(currentUser.UserID);

            ////pobriši flowlayoutpanel s projekti
            ////pojdi čez seznam projektov in ustvari kartice
            //flowLayoutPanel_ProjectTiles.Controls.Clear();
            //foreach (var p_item in projectsList)
            //{
            //    flowLayoutPanel_ProjectTiles.Controls.Add(new CustomProjectCardControl(p_item, this));
            //}
        }

        public void ShowAllProjectsOnOverviewTab()
        {
            // Overview Tab

            lbl_BoardOverview.Text = "Boards from " + currentUser.FullName;

            //Pridobi seznam projektov iz SQLite baze
            var projectsList = Project.GetAllProjectsList(currentUser.UserID);

            //pobriši flowlayoutpanel s projekti
            List<Control> listFlowLayoutControls = new List<Control>();
            foreach (Control control in flowLayoutPanel_ProjectTiles.Controls)
            {
                listFlowLayoutControls.Add(control);
            }
            foreach (Control ctrl in listFlowLayoutControls)
            {
                flowLayoutPanel_ProjectTiles.Controls.Remove(ctrl);
                ctrl.Dispose();
            }


            //pojdi čez seznam projektov in ustvari kartice
            foreach (var p_item in projectsList)
            {
                flowLayoutPanel_ProjectTiles.Controls.Add(new CustomProjectCardControl(p_item, this));
            }


        }


        private void btn_AddList_Click(object sender, EventArgs e)
        {
            flowLayoutPanel_CurrentBoardTab.Controls.Add(new CustomListControl()); //TODO
        }

        private void switchUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (UserSelectForm userSelectForm = new UserSelectForm())
            {
                //nastavi pozicijo okna
                userSelectForm.StartPosition = FormStartPosition.Manual;
                userSelectForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                // zamenjaj uporabnika
                if (userSelectForm.ShowDialog() == DialogResult.OK)
                {
                    //nastavi novega uporabnika iz select forme in prikaži vse projekte
                    currentUser = userSelectForm.selectedUser;

                    // prikaži prvi tab
                    SwitchTab(0);
                    ShowAllProjectsOnOverviewTab();


                    // pobriši CurrentBoard flowlayoutpanel s seznami
                    // nastavi label text , disable gumba za iskanje in nove liste
                    lbl_CurrentBoardTitle.Text = "Please select a board or create a new one";
                    btn_searchTab.Enabled = false;
                    btn_AddListTab.Enabled = false;

                    List<Control> listFlowLayoutControls = new List<Control>();
                    foreach (Control control in flowLayoutPanel_CurrentBoardTab.Controls)
                    {
                        listFlowLayoutControls.Add(control);
                    }
                    foreach (Control ctrl in listFlowLayoutControls)
                    {
                        flowLayoutPanel_CurrentBoardTab.Controls.Remove(ctrl);
                        ctrl.Dispose();
                    }
                }
            }
        }

        private void newUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frm_NewUserForm newUserForm = new frm_NewUserForm())
            {
                //nastavi pozicijo okna
                newUserForm.StartPosition = FormStartPosition.Manual;
                newUserForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                if (newUserForm.ShowDialog() == DialogResult.OK)
                {
                    //TODO vrni vrednosti

                    //kreiraj uporabnika
                }
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_OptionsForm optionsForm = new frm_OptionsForm();
            //nastavi pozicijo okna
            optionsForm.StartPosition = FormStartPosition.Manual;
            optionsForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

            optionsForm.ShowDialog();
            
        }


        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // shrani zadnjega uporabnika in izhod
            Properties.Settings.Default.LastUserID = currentUser.UserID;
            Properties.Settings.Default.Save();
            
            Application.Exit();
        }

        public void SwitchTab(int tabIndex)
        {
            tabControl_main.SelectedIndex = tabIndex;
        }

        // preberi vsebino boarda iz baze in prikaži v current tabu
        public void ShowProjectOnCurrentTab(Project proj)
        {
            //nastavi trenutni projekt, preberi iz baze vse ostale podatke (cardliste, todoje, ...)
            currentProject = proj.GetProject();

            //pobriši flowlayoutpanel s seznami
            List<Control> listFlowLayoutControls = new List<Control>();
            foreach (Control control in flowLayoutPanel_CurrentBoardTab.Controls)
            {
                listFlowLayoutControls.Add(control);
            }
            foreach (Control ctrl in listFlowLayoutControls)
            {
                flowLayoutPanel_CurrentBoardTab.Controls.Remove(ctrl);
                ctrl.Dispose();
            }


            // dodaj sezname v "currentProject" tabu
            foreach (CardList listItem in currentProject.CardList)
            {
                CustomListControl listControl = new CustomListControl(listItem, this);
                flowLayoutPanel_CurrentBoardTab.Controls.Add(listControl);
            }

            lbl_CurrentBoardTitle.Text = currentProject.Title;
            btn_AddListTab.Enabled = true;
            btn_searchTab.Enabled = true;
        }

        private void openBoardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ProjectSelectForm projectSelectForm = new ProjectSelectForm(currentUser, this)) 
            {
                //nastavi pozicijo okna
                projectSelectForm.StartPosition = FormStartPosition.Manual;
                projectSelectForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                if (projectSelectForm.ShowDialog() == DialogResult.OK)
                {
                    // izbran projekt prikaži
                }
            }
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void newBoardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (NewProjectForm newProjectForm = new NewProjectForm(currentUser))
            {
                //nastavi pozicijo okna
                newProjectForm.StartPosition = FormStartPosition.Manual;
                newProjectForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                if (newProjectForm.ShowDialog() == DialogResult.OK)
                {
                    // določi nov projekt
                    currentProject = newProjectForm.NewProject;

                    // pokliči metodo za osveževanje Overview Tab-a in Current Project Tab-a
                    ShowAllProjectsOnOverviewTab();
                    ShowProjectOnCurrentTab(currentProject);
                }
            }
        }

        private void exportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Izvozi trenutni projekt v TXT datoteko
            if (currentProject != null)
            {
                // odpri file dialog
                if (saveFileDialog_Export.ShowDialog() == DialogResult.OK)
                {
                    //posodobi currentproject
                    currentProject = currentProject.GetProject();
                    //začni izvoz projekta v txt
                    ExportBoard export = new ExportBoard(currentProject);
                    var exportedLines = export.FormatToText();
                    File.WriteAllLines(saveFileDialog_Export.FileName, exportedLines);

                    MessageBox.Show("Board was succesfully exported.", "Export Successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Currently no Board is selected. Please select a " +
                    "board on the main tab or by clicking File > Open Board.", "Export Board",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_AddListTab_Click(object sender, EventArgs e)
        {

            using (frm_NewListForm newListForm = new frm_NewListForm(currentProject))
            {    
                //nastavi pozicijo okna
                newListForm.StartPosition = FormStartPosition.Manual;
                newListForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                if (newListForm.ShowDialog() == DialogResult.OK)
                {
                    // znova preberi vse sezname
                    ShowProjectOnCurrentTab(currentProject);

                }
            }
        }
    }
}
