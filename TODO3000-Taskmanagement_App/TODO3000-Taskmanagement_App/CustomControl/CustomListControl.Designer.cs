﻿namespace TODO3000_Taskmanagement_App.CustomControl
{
    partial class CustomListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ListTitle = new System.Windows.Forms.Label();
            this.btn_ListAdd = new System.Windows.Forms.Button();
            this.panel_buttons = new System.Windows.Forms.Panel();
            this.btn_ListMore = new System.Windows.Forms.Button();
            this.flowLayoutPanel_ListCards = new System.Windows.Forms.FlowLayoutPanel();
            this.panel_buttons.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_ListTitle
            // 
            this.lbl_ListTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_ListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ListTitle.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_ListTitle.Location = new System.Drawing.Point(0, 0);
            this.lbl_ListTitle.MaximumSize = new System.Drawing.Size(250, 50);
            this.lbl_ListTitle.Name = "lbl_ListTitle";
            this.lbl_ListTitle.Padding = new System.Windows.Forms.Padding(5, 5, 5, 10);
            this.lbl_ListTitle.Size = new System.Drawing.Size(250, 50);
            this.lbl_ListTitle.TabIndex = 0;
            this.lbl_ListTitle.Text = "List Title Example";
            this.lbl_ListTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_ListTitle.Click += new System.EventHandler(this.lbl_ListTitle_Click);
            // 
            // btn_ListAdd
            // 
            this.btn_ListAdd.AutoSize = true;
            this.btn_ListAdd.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_ListAdd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ListAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ListAdd.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.btn_ListAdd.Location = new System.Drawing.Point(0, 0);
            this.btn_ListAdd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_ListAdd.Name = "btn_ListAdd";
            this.btn_ListAdd.Size = new System.Drawing.Size(205, 29);
            this.btn_ListAdd.TabIndex = 2;
            this.btn_ListAdd.Text = "+";
            this.btn_ListAdd.UseVisualStyleBackColor = false;
            this.btn_ListAdd.Click += new System.EventHandler(this.bnt_ListAdd_Click);
            // 
            // panel_buttons
            // 
            this.panel_buttons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_buttons.Controls.Add(this.btn_ListMore);
            this.panel_buttons.Controls.Add(this.btn_ListAdd);
            this.panel_buttons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_buttons.Location = new System.Drawing.Point(0, 50);
            this.panel_buttons.Name = "panel_buttons";
            this.panel_buttons.Size = new System.Drawing.Size(253, 29);
            this.panel_buttons.TabIndex = 3;
            // 
            // btn_ListMore
            // 
            this.btn_ListMore.AutoSize = true;
            this.btn_ListMore.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_ListMore.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_ListMore.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_ListMore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ListMore.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ListMore.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.btn_ListMore.Location = new System.Drawing.Point(205, 0);
            this.btn_ListMore.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.btn_ListMore.Name = "btn_ListMore";
            this.btn_ListMore.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.btn_ListMore.Size = new System.Drawing.Size(38, 29);
            this.btn_ListMore.TabIndex = 3;
            this.btn_ListMore.Text = "...";
            this.btn_ListMore.UseVisualStyleBackColor = false;
            this.btn_ListMore.Click += new System.EventHandler(this.btn_ListMore_Click);
            // 
            // flowLayoutPanel_ListCards
            // 
            this.flowLayoutPanel_ListCards.AutoSize = true;
            this.flowLayoutPanel_ListCards.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_ListCards.Location = new System.Drawing.Point(0, 79);
            this.flowLayoutPanel_ListCards.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.flowLayoutPanel_ListCards.MaximumSize = new System.Drawing.Size(500, 0);
            this.flowLayoutPanel_ListCards.MinimumSize = new System.Drawing.Size(250, 0);
            this.flowLayoutPanel_ListCards.Name = "flowLayoutPanel_ListCards";
            this.flowLayoutPanel_ListCards.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.flowLayoutPanel_ListCards.Size = new System.Drawing.Size(253, 517);
            this.flowLayoutPanel_ListCards.TabIndex = 5;
            this.flowLayoutPanel_ListCards.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel_ListCards_Paint);
            // 
            // CustomListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.flowLayoutPanel_ListCards);
            this.Controls.Add(this.panel_buttons);
            this.Controls.Add(this.lbl_ListTitle);
            this.Margin = new System.Windows.Forms.Padding(10);
            this.MinimumSize = new System.Drawing.Size(253, 500);
            this.Name = "CustomListControl";
            this.Size = new System.Drawing.Size(253, 596);
            this.Load += new System.EventHandler(this.CustomListControl_Load);
            this.SystemColorsChanged += new System.EventHandler(this.CustomListControl_SystemColorsChanged);
            this.panel_buttons.ResumeLayout(false);
            this.panel_buttons.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ListTitle;
        private System.Windows.Forms.Button btn_ListAdd;
        private System.Windows.Forms.Panel panel_buttons;
        private System.Windows.Forms.Button btn_ListMore;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_ListCards;
    }
}
