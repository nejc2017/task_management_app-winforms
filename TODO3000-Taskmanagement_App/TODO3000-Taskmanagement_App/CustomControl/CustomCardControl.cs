﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.Tables;
using TODO3000_Taskmanagement_App.CustomForms;

namespace TODO3000_Taskmanagement_App.CustomControl
{
    public partial class CustomCardControl : UserControl
    {
        ToDo _todo;
        CustomListControl customList;

        public CustomCardControl(ToDo todoItem, CustomListControl thisList)
        {
            InitializeComponent();

            _todo = todoItem;
            customList = thisList;

            lbl_title.Text = _todo.Title;
            lbl_Description.Text = _todo.Description;

            //lbl info
            string isFinished = "✓ ";
            if (todoItem.DateFinished != DateTime.MinValue)
            {
                lbl_isFinished.Text = isFinished;
            }
            else
            {
                lbl_isFinished.Text = "";
            }
            lbl_info.Text = todoItem.DateCreated.ToShortDateString();
        }

        private void Refresh(ToDo todoItem)
        {

            _todo = todoItem;
            lbl_title.Text = todoItem.Title;
            lbl_Description.Text = todoItem.Description;

            //lbl info
            string isFinished = "✓ ";
            if (todoItem.DateFinished != DateTime.MinValue)
            {
                lbl_isFinished.Text = isFinished;
            }
            else
            {
                lbl_isFinished.Text = "";
            }
            lbl_info.Text = todoItem.DateCreated.ToShortDateString();
        }


        private void btn_CardEdit_Click(object sender, EventArgs e)
        {
            // odpri Edit formo za urejanje
            using (frm_EditCardForm editCardForm = new frm_EditCardForm(_todo))
            {
                //nastavi pozicijo okna
                editCardForm.StartPosition = FormStartPosition.Manual;
                editCardForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                //prikažeš formo, uporabnik lahko spremeni vrednosti, ko klikne save vrni "OK"
                if (editCardForm.ShowDialog() == DialogResult.OK)
                {
                    // če uporabnik klikne Shrani, prikaži spremembe
                    this.Refresh(editCardForm.todo);

                }
                else if (editCardForm.DialogResult == DialogResult.Yes)
                {
                    customList.RefreshProject();
                }
            }
        }

        private void CustomCardControl_Load(object sender, EventArgs e)
        {
            lbl_title.ForeColor = Properties.Settings.Default.CardTitleColor;
            lbl_Description.ForeColor = Properties.Settings.Default.CardDescriptionColor;


        }
    }
}
