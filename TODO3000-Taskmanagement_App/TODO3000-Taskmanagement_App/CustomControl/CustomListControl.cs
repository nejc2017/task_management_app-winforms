﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App.CustomForms;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomControl
{
    public partial class CustomListControl : UserControl
    {
        int _listID = 1;
        int panelHeight;
        CardList CardList;
        frm_TodoMain mainFrm;

        public CustomListControl()
        {
            InitializeComponent();
        }

        public CustomListControl(int listID)
        {
            InitializeComponent();
            _listID = listID;

            List<Tables.ToDo> todoList = new List<Tables.ToDo>();
            todoList = Tables.CardList.GetTodoByListId(listID);

            foreach (var todo in todoList)
            {
                // ustvari novo kontrolo za kartico
                // napolni jo z vrednostmi iz objekta poslanega preko forme
                CustomCardControl card = new CustomCardControl(todo, this);

                FillList(card);
            }

        }

        public void FillList(CustomCardControl card)
        {
            // preveri velikost Cardlista s pomočjo števila kartic v flowlayoutpanelu
            // nastavi cardlist velikost
            int noCards = 1 + flowLayoutPanel_ListCards.Controls.Count;
            int panelHeight = ((card.Height + card.Margin.Top + card.Margin.Bottom + flowLayoutPanel_ListCards.Padding.Top + flowLayoutPanel_ListCards.Padding.Bottom) * noCards)
                        + lbl_ListTitle.Height + lbl_ListTitle.Padding.Top + lbl_ListTitle.Padding.Bottom
                        + panel_buttons.Height + panel_buttons.Padding.Top + panel_buttons.Padding.Bottom;

            flowLayoutPanel_ListCards.Controls.Add(card);
            // nastavi velikost 
            this.Height = panelHeight + 50;
            flowLayoutPanel_ListCards.Height = panelHeight + 50;
        }

        //naredi seznam in ga napolni s karticami
        public CustomListControl(CardList cList, frm_TodoMain frm )
        {
            InitializeComponent();
            CardList = cList;
            mainFrm = frm;

            List<Tables.ToDo> todoList = new List<Tables.ToDo>();
            todoList = Tables.CardList.GetTodoByListId(CardList.ListID);

            foreach (var todo in todoList)
            {
                // ustvari novo kontrolo za kartico
                // napolni jo z vrednostmi iz objekta poslanega preko forme
                CustomCardControl card = new CustomCardControl(todo, this);

                FillList(card);
            }

        }

        private void contextMenuStrip_ListControlMore_Opening(object sender, CancelEventArgs e)
        {

        }

        private void btn_ListMore_Click(object sender, EventArgs e)
        {
            //odpremo formo za urejanje CardListe
            using (frm_EditListForm editListForm = new frm_EditListForm(CardList))
            {
                //nastavi pozicijo okna
                editListForm.StartPosition = FormStartPosition.Manual;
                editListForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                if (editListForm.ShowDialog() == DialogResult.OK)
                {
                    this.lbl_ListTitle.Text = editListForm.CardList.Title;
                }
                else if (editListForm.DialogResult == DialogResult.Yes)
                {
                    mainFrm.ShowProjectOnCurrentTab(mainFrm.currentProject);
                }
            }
        }

        private void bnt_ListAdd_Click(object sender, EventArgs e)
        {

            // Kreiranje nove todo kartice
            // odpri formo za nove kartice
            using (frm_NewCardForm newCardForm = new frm_NewCardForm(CardList))
            {
                // prikaži formo uporabniku
                //nastavi pozicijo okna
                newCardForm.StartPosition = FormStartPosition.Manual;
                newCardForm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);

                // preveri kaj uporabnik klikne: ok/add da se ustvari nova kartica, ali prekliče izbiro
                if (newCardForm.ShowDialog() == DialogResult.OK)
                {
                    // ustvari novo kontrolo za kartico
                    // napolni jo z vrednostmi iz objekta poslanega preko forme
                    CustomCardControl card = new CustomCardControl(newCardForm.CreatedTodo, this);

                    // preveri velikost Cardlista s pomočjo števila kartic v flowlayoutpanelu
                    // nastavi cardlist velikost
                    int noCards = 1 + flowLayoutPanel_ListCards.Controls.Count;
                    int panelHeight = ((card.Height + card.Margin.Top + card.Margin.Bottom + flowLayoutPanel_ListCards.Padding.Top) * noCards)
                        + lbl_ListTitle.Height + lbl_ListTitle.Padding.Top + lbl_ListTitle.Padding.Bottom
                        + panel_buttons.Height + panel_buttons.Padding.Top + panel_buttons.Padding.Bottom;
                    // dodaj kontrolo na navpični panel (listo)
                    flowLayoutPanel_ListCards.Controls.Add(card);
                    this.Height = panelHeight + 50;
                    flowLayoutPanel_ListCards.Height = panelHeight + 50; // magic number, popravi
                }
            }


        }



        private void CustomListControl_Load(object sender, EventArgs e)
        {
            // Naslov
            lbl_ListTitle.Text = CardList.Title;

            // nastavi velikost glede na število kartic 
            // število kartic se izračuan v FillList()

            //nastavimo barvo
            lbl_ListTitle.ForeColor = Properties.Settings.Default.ListTitleColor;

        }

        private void flowLayoutPanel_ListCards_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CustomListControl_SystemColorsChanged(object sender, EventArgs e)
        {

        }

        private void lbl_ListTitle_Click(object sender, EventArgs e)
        {

        }
        public void RefreshProject()
        {
            mainFrm.ShowProjectOnCurrentTab(mainFrm.currentProject);

        }
    }
}
