﻿namespace TODO3000_Taskmanagement_App.CustomControl
{
    partial class CustomProjectCardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ProjectTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_ProjectTitle
            // 
            this.lbl_ProjectTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_ProjectTitle.AutoEllipsis = true;
            this.lbl_ProjectTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.lbl_ProjectTitle.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_ProjectTitle.Location = new System.Drawing.Point(3, 64);
            this.lbl_ProjectTitle.Name = "lbl_ProjectTitle";
            this.lbl_ProjectTitle.Padding = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.lbl_ProjectTitle.Size = new System.Drawing.Size(244, 36);
            this.lbl_ProjectTitle.TabIndex = 0;
            this.lbl_ProjectTitle.Text = "Project Title";
            this.lbl_ProjectTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lbl_ProjectTitle.Click += new System.EventHandler(this.SelectProject_Click);
            // 
            // CustomProjectCardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbl_ProjectTitle);
            this.Margin = new System.Windows.Forms.Padding(10);
            this.MaximumSize = new System.Drawing.Size(250, 100);
            this.MinimumSize = new System.Drawing.Size(250, 100);
            this.Name = "CustomProjectCardControl";
            this.Size = new System.Drawing.Size(250, 100);
            this.Load += new System.EventHandler(this.CustomProjectCardControl_Load);
            this.Click += new System.EventHandler(this.SelectProject_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_ProjectTitle;
    }
}
