﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TODO3000_Taskmanagement_App;
using TODO3000_Taskmanagement_App.Tables;

namespace TODO3000_Taskmanagement_App.CustomControl
{
    public partial class CustomProjectCardControl : UserControl
    {
        Project cardProject;
        frm_TodoMain formMain;
        public CustomProjectCardControl()
        {
            InitializeComponent();
            // naključna barva ozadnja se pridobi iz seznama izbranih barv
            this.BackColor = Color.FromKnownColor(ColorRandomizer.RandomColor());
        }

        public CustomProjectCardControl(Project project)
        {
            InitializeComponent();
            // naključna barva ozadnja se pridobi iz seznama izbranih barv
            this.BackColor = Color.FromKnownColor(ColorRandomizer.RandomColor());
            cardProject = project;

            lbl_ProjectTitle.Text = cardProject.Title;

        }

        public CustomProjectCardControl(Project project, Form parentForm)
        {
            InitializeComponent();
            // naključna barva ozadnja se pridobi iz seznama izbranih barv
            this.BackColor = Color.FromKnownColor(ColorRandomizer.RandomColor());
            cardProject = project;

            lbl_ProjectTitle.Text = cardProject.Title;

            // poslana je glavna forma (da lahko menjamo tabe, ...)
            formMain = (frm_TodoMain)parentForm;

        }
        private void CustomProjectCardControl_Load(object sender, EventArgs e)
        {

        }

        private void SelectProject_Click(object sender, EventArgs e)
        {
            // sproži tabswitch event
            formMain.SwitchTab(1);
            // pošlji izbrani projekt v glavno formo, da se dodajo kartice in seznami
            formMain.ShowProjectOnCurrentTab(cardProject);

        }

        private void lbl_shadowTitle_Click(object sender, EventArgs e)
        {

        }
    }
}
