﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks_app.Dialog
{
    class About
    {
        //shows the "About" messagebox from the menustrip
        public void Show()
        {
            string text = "Tasks App v0.1,\nBy: Nejc, 2020.8.8";
            DialogResult aboutDialog = MessageBox.Show(text, "About", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

    }
}
