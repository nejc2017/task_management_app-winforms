﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks_app.Dialog
{
    class CardListForm : Form
    {
        public string ListTitle { get; set; }
        public Form CardForm { get; set; }
        public CardListForm()
        {
            Form cardListEnterNameForm = new Form()
            {

                FormBorderStyle = FormBorderStyle.FixedSingle,
                AutoSize = true,
                AutoSizeMode = AutoSizeMode.GrowAndShrink,
                StartPosition = FormStartPosition.CenterParent,
                Text = "New List"

            };


            Label lbl_CardList = new Label()
            {
                Text = "Enter list title:",
                AutoSize = true,
                Anchor = AnchorStyles.None
            };
            TextBox tb_CardTitle = new TextBox()
            {
                Anchor = AnchorStyles.None
            };

            
            Button btn_Ok = new Button()
            {
                Text = "Create list",
                Anchor = AnchorStyles.Top, 
            };
            
            TableLayoutPanel tableLayout = new TableLayoutPanel()
            {
                RowCount = 2,
                ColumnCount = 2,
                Location = new System.Drawing.Point(0, 0),
            };
            //create a table layout for controls (grid system)
            cardListEnterNameForm.Controls.Add(tableLayout);
            
            //add controls and set row height
            tableLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayout.Controls.Add(tb_CardTitle, 0, 0); 
            tableLayout.Controls.Add(lbl_CardList, 0, 0);
            tableLayout.Controls.Add(btn_Ok,0,1); //second row, 1st cell

            //bind the button event to send the card title
            btn_Ok.Click += (sender, EventArgs) => {
                btn_Ok_Click(sender, EventArgs, tb_CardTitle.Text);
                };
            CardForm = cardListEnterNameForm;
            cardListEnterNameForm.ShowDialog();

        }

        private void btn_Ok_Click(object sender, EventArgs e, string labelText)
        {
            if (string.IsNullOrEmpty(labelText))
            {
                DialogResult incorrectTitle = MessageBox.Show("Please enter a name for the list or close the window", 
                    "Missing title",
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
            DialogResult = DialogResult.OK;
            ListTitle = labelText;
            CardForm.Close();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CardListForm
            // 
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Name = "CardListForm";
            this.ResumeLayout(false);

        }
    }
}
