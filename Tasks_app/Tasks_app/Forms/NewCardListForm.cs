﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tasks_app.Model;

namespace Tasks_app.UserControls
{
    public partial class NewCardListForm : Form
    {
        public CardList cardList;

        public NewCardListForm()
        {
            InitializeComponent();
            cardList = new CardList();
        }

        private void lbl_CardListName_Click(object sender, EventArgs e)
        {

        }

        private void btn_CreateCardList_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                FillObject();
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.None;
            }
        }

        private void FillObject()
        {
            cardList.Title = tb_cardList.Text;
        }

        private bool ValidateForm()
        {
            bool valid = true;

            if (string.IsNullOrEmpty(tb_cardList.Text))
            {
                valid = false;
                errorProviderCardList.SetError(tb_cardList, "Please enter a valid card list title");
            }
            else
            {
                errorProviderCardList.SetError(tb_cardList, "");

            }
            return valid;
        }

        private void TestnaMetoda(object sender, EventArgs e)
        {

        }
    }
}
