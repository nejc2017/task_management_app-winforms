﻿namespace Tasks_app.UserControls
{
    partial class NewCardListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tb_cardList = new System.Windows.Forms.TextBox();
            this.lbl_CardListName = new System.Windows.Forms.Label();
            this.btn_CreateCardList = new System.Windows.Forms.Button();
            this.btn_CancelCardList = new System.Windows.Forms.Button();
            this.errorProviderCardList = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCardList)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_cardList
            // 
            this.tb_cardList.Location = new System.Drawing.Point(146, 23);
            this.tb_cardList.Name = "tb_cardList";
            this.tb_cardList.Size = new System.Drawing.Size(156, 22);
            this.tb_cardList.TabIndex = 0;
            // 
            // lbl_CardListName
            // 
            this.lbl_CardListName.AutoSize = true;
            this.lbl_CardListName.Location = new System.Drawing.Point(36, 23);
            this.lbl_CardListName.Name = "lbl_CardListName";
            this.lbl_CardListName.Size = new System.Drawing.Size(103, 17);
            this.lbl_CardListName.TabIndex = 1;
            this.lbl_CardListName.Text = "Card List name";
            this.lbl_CardListName.Click += new System.EventHandler(this.lbl_CardListName_Click);
            // 
            // btn_CreateCardList
            // 
            this.btn_CreateCardList.Location = new System.Drawing.Point(146, 51);
            this.btn_CreateCardList.Name = "btn_CreateCardList";
            this.btn_CreateCardList.Size = new System.Drawing.Size(75, 23);
            this.btn_CreateCardList.TabIndex = 2;
            this.btn_CreateCardList.Text = "Create";
            this.btn_CreateCardList.UseVisualStyleBackColor = true;
            this.btn_CreateCardList.Click += new System.EventHandler(this.btn_CreateCardList_Click);
            // 
            // btn_CancelCardList
            // 
            this.btn_CancelCardList.Location = new System.Drawing.Point(227, 51);
            this.btn_CancelCardList.Name = "btn_CancelCardList";
            this.btn_CancelCardList.Size = new System.Drawing.Size(75, 23);
            this.btn_CancelCardList.TabIndex = 3;
            this.btn_CancelCardList.Text = "Cancel";
            this.btn_CancelCardList.UseVisualStyleBackColor = true;
            // 
            // errorProviderCardList
            // 
            this.errorProviderCardList.ContainerControl = this;
            // 
            // NewCardListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(342, 92);
            this.Controls.Add(this.btn_CancelCardList);
            this.Controls.Add(this.btn_CreateCardList);
            this.Controls.Add(this.lbl_CardListName);
            this.Controls.Add(this.tb_cardList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "NewCardListForm";
            this.Text = "New card list";
            this.Load += new System.EventHandler(this.TestnaMetoda);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCardList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_cardList;
        private System.Windows.Forms.Label lbl_CardListName;
        private System.Windows.Forms.Button btn_CreateCardList;
        private System.Windows.Forms.Button btn_CancelCardList;
        private System.Windows.Forms.ErrorProvider errorProviderCardList;
    }
}