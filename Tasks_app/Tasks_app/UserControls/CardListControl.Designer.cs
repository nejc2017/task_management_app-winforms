﻿namespace Tasks_app.UserControls
{
    partial class CardListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowCardListButtons = new System.Windows.Forms.FlowLayoutPanel();
            this.btnDeleteCardList = new System.Windows.Forms.Button();
            this.btnListToRight = new System.Windows.Forms.Button();
            this.btnListToLeft = new System.Windows.Forms.Button();
            this.btnAddCard = new System.Windows.Forms.Button();
            this.gboxCardList = new System.Windows.Forms.GroupBox();
            this.flowCardListButtons.SuspendLayout();
            this.gboxCardList.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowCardListButtons
            // 
            this.flowCardListButtons.AutoSize = true;
            this.flowCardListButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowCardListButtons.Controls.Add(this.btnDeleteCardList);
            this.flowCardListButtons.Controls.Add(this.btnListToRight);
            this.flowCardListButtons.Controls.Add(this.btnListToLeft);
            this.flowCardListButtons.Controls.Add(this.btnAddCard);
            this.flowCardListButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowCardListButtons.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowCardListButtons.Location = new System.Drawing.Point(3, 18);
            this.flowCardListButtons.Name = "flowCardListButtons";
            this.flowCardListButtons.Size = new System.Drawing.Size(244, 26);
            this.flowCardListButtons.TabIndex = 1;
            // 
            // btnDeleteCardList
            // 
            this.btnDeleteCardList.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteCardList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCardList.Image = global::Tasks_app.Properties.Resources.img_close;
            this.btnDeleteCardList.Location = new System.Drawing.Point(221, 3);
            this.btnDeleteCardList.Name = "btnDeleteCardList";
            this.btnDeleteCardList.Size = new System.Drawing.Size(20, 20);
            this.btnDeleteCardList.TabIndex = 3;
            this.btnDeleteCardList.UseVisualStyleBackColor = true;
            // 
            // btnListToRight
            // 
            this.btnListToRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListToRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListToRight.Image = global::Tasks_app.Properties.Resources.GlyphRight_16x;
            this.btnListToRight.Location = new System.Drawing.Point(195, 3);
            this.btnListToRight.Name = "btnListToRight";
            this.btnListToRight.Size = new System.Drawing.Size(20, 20);
            this.btnListToRight.TabIndex = 2;
            this.btnListToRight.UseVisualStyleBackColor = true;
            // 
            // btnListToLeft
            // 
            this.btnListToLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListToLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListToLeft.Image = global::Tasks_app.Properties.Resources.GlyphLeft_16x;
            this.btnListToLeft.Location = new System.Drawing.Point(169, 3);
            this.btnListToLeft.Name = "btnListToLeft";
            this.btnListToLeft.Size = new System.Drawing.Size(20, 20);
            this.btnListToLeft.TabIndex = 1;
            this.btnListToLeft.UseVisualStyleBackColor = true;
            this.btnListToLeft.Click += new System.EventHandler(this.btnListToLeft_Click);
            // 
            // btnAddCard
            // 
            this.btnAddCard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCard.Image = global::Tasks_app.Properties.Resources.ASX_Add_grey_16x;
            this.btnAddCard.Location = new System.Drawing.Point(143, 3);
            this.btnAddCard.Name = "btnAddCard";
            this.btnAddCard.Size = new System.Drawing.Size(20, 20);
            this.btnAddCard.TabIndex = 0;
            this.btnAddCard.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAddCard.UseVisualStyleBackColor = true;
            this.btnAddCard.Click += new System.EventHandler(this.btnAddCard_Click);
            // 
            // gboxCardList
            // 
            this.gboxCardList.AutoSize = true;
            this.gboxCardList.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gboxCardList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gboxCardList.Controls.Add(this.flowCardListButtons);
            this.gboxCardList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxCardList.Location = new System.Drawing.Point(0, 0);
            this.gboxCardList.MinimumSize = new System.Drawing.Size(250, 480);
            this.gboxCardList.Name = "gboxCardList";
            this.gboxCardList.Size = new System.Drawing.Size(250, 480);
            this.gboxCardList.TabIndex = 2;
            this.gboxCardList.TabStop = false;
            this.gboxCardList.Text = "groupBox1";
            this.gboxCardList.Enter += new System.EventHandler(this.gboxCardList_Enter);
            // 
            // CardListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.gboxCardList);
            this.MaximumSize = new System.Drawing.Size(250, 0);
            this.MinimumSize = new System.Drawing.Size(250, 480);
            this.Name = "CardListControl";
            this.Size = new System.Drawing.Size(250, 480);
            this.flowCardListButtons.ResumeLayout(false);
            this.gboxCardList.ResumeLayout(false);
            this.gboxCardList.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddCard;
        private System.Windows.Forms.FlowLayoutPanel flowCardListButtons;
        private System.Windows.Forms.GroupBox gboxCardList;
        private System.Windows.Forms.Button btnListToLeft;
        private System.Windows.Forms.Button btnListToRight;
        private System.Windows.Forms.Button btnDeleteCardList;
    }
}
