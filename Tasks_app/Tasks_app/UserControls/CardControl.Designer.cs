﻿namespace Tasks_app.UserControls
{
    partial class CardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgridvCardText = new System.Windows.Forms.DataGridView();
            this.CardColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgridvCardText)).BeginInit();
            this.SuspendLayout();
            // 
            // dgridvCardText
            // 
            this.dgridvCardText.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgridvCardText.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgridvCardText.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgridvCardText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgridvCardText.ColumnHeadersHeight = 29;
            this.dgridvCardText.ColumnHeadersVisible = false;
            this.dgridvCardText.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CardColumn});
            this.dgridvCardText.GridColor = System.Drawing.SystemColors.Control;
            this.dgridvCardText.Location = new System.Drawing.Point(4, 4);
            this.dgridvCardText.Name = "dgridvCardText";
            this.dgridvCardText.RowHeadersWidth = 51;
            this.dgridvCardText.RowTemplate.Height = 24;
            this.dgridvCardText.Size = new System.Drawing.Size(240, 150);
            this.dgridvCardText.TabIndex = 0;
            // 
            // CardColumn
            // 
            this.CardColumn.HeaderText = "Title Example";
            this.CardColumn.MinimumWidth = 6;
            this.CardColumn.Name = "CardColumn";
            // 
            // CardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgridvCardText);
            this.Name = "CardControl";
            this.Size = new System.Drawing.Size(240, 150);
            ((System.ComponentModel.ISupportInitialize)(this.dgridvCardText)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgridvCardText;
        private System.Windows.Forms.DataGridViewTextBoxColumn CardColumn;
    }
}
