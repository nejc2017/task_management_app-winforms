﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_app.Model
{
    class Card
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool isDone { get; set; }
        public DateTime DateAdded { get; set; }

        //TODO List<Comment>
    }
}
