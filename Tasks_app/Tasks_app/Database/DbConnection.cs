﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_app.Database
{
    class DbConnection
    {
        string connString = "";
        public DataSet ReadData()
        {
            
            DataSet dataset = new DataSet();
            var sql = "SELECT * FROM TodoItems";

            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand sqlcmd = new SqlCommand(sql, conn))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlcmd))
                    {
                        adapter.Fill(dataset);
                    }
                }
            }
            return dataset;
        }

        //https://stackoverflow.com/questions/45306032/how-do-i-create-a-class-with-sql-connection-and-query-functions-and-calling-it-t

        public void Delete(string itemId)
        {
            var sql = "DELETE FROM TodoItems where Id = @Id";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand sqlcmd = new SqlCommand(sql, conn)) 
                {
                    sqlcmd.Parameters.Add("@Id", SqlDbType.Int).Value = itemId;
                    conn.Open();
                    sqlcmd.ExecuteNonQuery();
                }
            }

        }

    }
}
