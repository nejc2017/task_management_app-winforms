﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Tasks_app.Model;
using Tasks_app.UserControls;

namespace Tasks_app
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void toggleFullscreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fullscreen fullscreen = new Fullscreen();
            if (this.WindowState == FormWindowState.Normal)
            {
                fullscreen.EnterFullscreen(this);
            }
            else
            {
                fullscreen.LeaveFullscreen(this);
            }
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)
            {
                Fullscreen fullscreen = new Fullscreen();
                if (this.WindowState == FormWindowState.Normal)
                {
                    fullscreen.EnterFullscreen(this);
                }
                else
                {
                    fullscreen.LeaveFullscreen(this);
                }
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Dialog.About aboutDialog = new Dialog.About();
            aboutDialog.Show();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // TODO Check if any changes were saved before exiting

            // ask before closing, to avoid unintentionally exiting
            DialogResult quit = MessageBox.Show("Are you sure you want to quit?", "Quit?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (quit == DialogResult.Yes)
            {
                Application.Exit();
            }


        }

        private void toolBtn_AddColumn_Click(object sender, EventArgs e)
        {
            //commented because of the below alternative solution
            //ProjectManager projectm = new ProjectManager();
            //projectm.NewCardListCreated += new EventHandler<Model.CardListEventArgs>(AddCardList_OnCardListCreated);
            //projectm.CreateCardList();

            //register the event from the pmanager to create a cardlist


            using (NewCardListForm nCardListFrm = new NewCardListForm())
            {
                if (nCardListFrm.ShowDialog() == DialogResult.OK)
                {
                    var nCardList = new CardListControl(nCardListFrm.cardList);
                    
                    flowLayoutPanel.Controls.Add(nCardList);
                    
                }
            }
        }

        //private void AddCardList_OnCardListCreated(object sender, CardListEventArgs e)
        //{
        //    //create the gui control in the main form
        //    GroupBox gbox = new GroupBox() {
        //        Text = e.CardList.Title,
        //        Size = new Size(150, 400),
        //    };
        //    FlowLayoutPanel flow_Buttons = new FlowLayoutPanel() {
        //        FlowDirection = FlowDirection.RightToLeft,
        //        AutoSize = true,
        //        AutoSizeMode = AutoSizeMode.GrowAndShrink,
        //        Dock = DockStyle.Fill
        //        };
        //    Button btn_Remove = new Button() {
        //        Text = "",
        //        Image = Properties.Resources.img_close,
        //        Size = new Size(20, 20),
        //        Margin = new Padding(0)};
        //    Button btn_AddCard = new Button()
        //    {
        //        Text = "",
        //        Image = Properties.Resources.Add_16x,
        //        Size = new Size(20, 20),
        //        Margin = new Padding(0)
        //    };
        //    Button btn_Left = new Button() {
        //        Text = "",
        //        Image = Properties.Resources.GlyphLeft_16x,
        //        Size = new Size(20, 20),
        //        Margin = new Padding(0)
        //    };
        //    Button btn_Right = new Button() {
        //        Text = "",
        //        Image= Properties.Resources.GlyphRight_16x,
        //        Size = new Size(20, 20),
        //        Margin = new Padding(0)
        //    };
        //    flow_Buttons.Controls.Add(btn_Remove);
        //    flow_Buttons.Controls.Add(btn_AddCard);
        //    flow_Buttons.Controls.Add(btn_Right);
        //    flow_Buttons.Controls.Add(btn_Left);
        //    //add the flow layout with the buttons to the top of the group box and fill it and autosize
        //    gbox.Controls.Add(flow_Buttons);
        //    //add to the main form flowlayout
        //    flowLayoutPanel.Controls.Add(gbox);

        //    btn_AddCard.Click += AddCard_Click;

            
       
        //}

        private void groupBox_Project_Enter(object sender, EventArgs e)
        {

            //define a new objectlistview and the columns
            //http://objectlistview.sourceforge.net/cs/ownerDraw.html
            ObjectListView olv_card = new ObjectListView()
            {
                //HeaderStyle = ColumnHeaderStyle.None,
                OwnerDraw = true, // set to true, to allow word wrap into multiple lines
                View = View.Tile,
                ShowGroups = false,
                Scrollable = false,
                Size = new Size(150, 100),
                TileSize = new Size(150, 75),
                RowHeight = 32, //word wrap
            };
            var titleColumn = new OLVColumn()
            {
                IsTileViewColumn = true,
            };
            var descriptionColumn = new OLVColumn() { IsTileViewColumn = true, WordWrap = true, TextAlign = HorizontalAlignment.Center, HeaderTextAlign = HorizontalAlignment.Center };
            //https://stackoverflow.com/questions/10075111/objectlistview-doesnt-word-wrap
            //https://stackoverflow.com/questions/2653781/how-to-wordwrap-the-text-in-a-column-using-objectlistview/2661416
            //descriptionColumn.Renderer = new BaseRenderer();
            //((BaseRenderer)descriptionColumn.Renderer).CanWrap = true;
            //((BaseRenderer)descriptionColumn.Renderer).UseGdiTextRendering = false;

            var dateAddedColumn = new OLVColumn() { IsTileViewColumn = true, TextAlign = HorizontalAlignment.Center };

            //add columns to the olv_card
            olv_card.AllColumns.Add(titleColumn);
            olv_card.AllColumns.Add(descriptionColumn);
            olv_card.AllColumns.Add(dateAddedColumn);

            //define aspect name with the same names as in the "Card" class
            titleColumn.AspectName = "Title";
            descriptionColumn.AspectName = "Description";
            dateAddedColumn.AspectName = "DateAdded";

            titleColumn.Text = "text";
            descriptionColumn.Text = "desc";
            dateAddedColumn.Text = "date";


            //add header columns
            olv_card.Columns.AddRange(new ColumnHeader[] { titleColumn, descriptionColumn, dateAddedColumn });

            //create an example card, which is added to a list
            //assign the list to the OLV
            Card exampleCard = new Card() { Title = "title example", Description = "this is a lengthy description of the card, which will probably require a line break", DateAdded = new DateTime(2020, 8, 15) };
            Card exampleCard2 = new Card() { Title = "title 2 example", Description = "this is a lengthy description of the card, which will probably require a line break", DateAdded = new DateTime(2020, 8, 15) };
            var cardList = new List<Card>() { exampleCard, exampleCard2 };
            olv_card.SetObjects(cardList);


        }

        private void toolBtn_AddCard_Click(object sender, EventArgs e)
        {
        }

        private void AddCard_Click(object sender, EventArgs e)
        {

            //define a new objectlistview and the columns
            //http://objectlistview.sourceforge.net/cs/ownerDraw.html
            ObjectListView olv_card = new ObjectListView()
            {
                //HeaderStyle = ColumnHeaderStyle.None,
                OwnerDraw = true, // set to true, to allow word wrap into multiple lines
                View = View.Tile,
                ShowGroups = false,
                Scrollable = false,
                Size = new Size(150, 100),
                TileSize = new Size(150, 75),
                RowHeight = 32, //word wrap
            };
            var titleColumn = new OLVColumn()
            {
                IsTileViewColumn = true,
            };
            var descriptionColumn = new OLVColumn() { IsTileViewColumn = true, WordWrap = true, TextAlign = HorizontalAlignment.Center, HeaderTextAlign = HorizontalAlignment.Center };
            //https://stackoverflow.com/questions/10075111/objectlistview-doesnt-word-wrap
            //https://stackoverflow.com/questions/2653781/how-to-wordwrap-the-text-in-a-column-using-objectlistview/2661416
            //descriptionColumn.Renderer = new BaseRenderer();
            //((BaseRenderer)descriptionColumn.Renderer).CanWrap = true;
            //((BaseRenderer)descriptionColumn.Renderer).UseGdiTextRendering = false;

            var dateAddedColumn = new OLVColumn() { IsTileViewColumn = true, TextAlign = HorizontalAlignment.Center };

            //add columns to the olv_card
            olv_card.AllColumns.Add(titleColumn);
            olv_card.AllColumns.Add(descriptionColumn);
            olv_card.AllColumns.Add(dateAddedColumn);

            //define aspect name with the same names as in the "Card" class
            titleColumn.AspectName = "Title";
            descriptionColumn.AspectName = "Description";
            dateAddedColumn.AspectName = "DateAdded";

            titleColumn.Text = "text";
            descriptionColumn.Text = "desc";
            dateAddedColumn.Text = "date";


            //add header columns
            olv_card.Columns.AddRange(new ColumnHeader[] { titleColumn, descriptionColumn, dateAddedColumn });

            //create an example card, which is added to a list
            //assign the list to the OLV
            Card exampleCard = new Card() { Title = "title example", Description = "this is a lengthy description of the card, which will probably require a line break", DateAdded = new DateTime(2020, 8, 15) };

            var cardList = new List<Card>() { exampleCard };
            olv_card.SetObjects(cardList);


            //add the OLV to the groupbox of the cardList column
            //how to add the card control to gbox
            flowLayoutPanel.Controls.Add(olv_card);
        }

        private void flowLayoutPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
